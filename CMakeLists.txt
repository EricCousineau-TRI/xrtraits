# Copyright 2018-2019, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

cmake_minimum_required(VERSION 3.9.0)
project(xrtraits VERSION 0.1.0)

###
# Detect if we're nested inside another project and should thus be very very quiet.
###
if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")
    set(XRTRAITS_IS_SUBPROJECT OFF)
    set(XRTRAITS_IS_STANDALONE_PROJECT ON)
    function(xrtraits_standalone_message)
        message(${ARGN})
    endfunction()
else()
    set(XRTRAITS_IS_SUBPROJECT ON)
    set(XRTRAITS_IS_STANDALONE_PROJECT OFF)
    function(xrtraits_standalone_message)
        # Do not print messages if we're a subproject
    endfunction()
endif()

###
# CMake modules
###
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(CMakeDependentOption)
include(Coverage)

###
# General compiler/linker settings
###
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

###
# Find dependencies - mostly optional
###
find_package(OpenXR QUIET COMPONENTS specscripts sdkscripts headers registry)
find_package(PythonInterp 3 QUIET)
find_package(CXXGSL)
find_program(CLANGTIDY_COMMAND
    NAMES
    clang-tidy
    clang-tidy-9
    clang-tidy-8
    clang-tidy-7
    clang-tidy-6.0)
# Don't look for fmt if there is already a fmt target
if(NOT TARGET fmt)
    find_package(fmt QUIET)
endif()
if(TARGET fmt OR TARGET fmt::fmt)
    set(FMT_FOUND ON)
else()
    set(FMT_FOUND OFF)
endif()

###
# Options
###
cmake_dependent_option(BUILD_WITH_GSL "Should this library build against the Guideline Support Library?" OFF
    "CXXGSL_FOUND" OFF)
cmake_dependent_option(BUILD_WITH_EXCEPTIONS
    "Should this library build with the exceptions namespace? Requires fmtlib, and is the only part to actually require compilation" ON
    "FMT_FOUND" OFF)
option(BUILD_WITH_CXX14
    "Should the C++14-only projects be compiled and targets be exported?" ON)
cmake_dependent_option(BUILD_WITH_CXX17
    "Should the C++17-depending projects be compiled and targets be exported?" ON
    "cxx_std_17 IN_LIST CMAKE_CXX_COMPILE_FEATURES" OFF)

if(OpenXR_specscripts_FOUND AND OpenXR_registry_FOUND AND OPENXR_PLATFORM_DEFINES_INCLUDE_DIR AND PYTHON_EXECUTABLE)
    option(BUILD_GENERATED_OPENXR_HEADERS "Should OpenXR headers be generated instead of using locally-cached versions?" ON)
else()
    set(BUILD_GENERATED_OPENXR_HEADERS OFF)
endif()

if(OpenXR_registry_FOUND AND PYTHON_EXECUTABLE)
    option(BUILD_GENERATED_TRAITS "Should generated files be generated instead of using locally-cached versions?" ON)
    # option(BUILD_UPDATE_GENERATED_TRAITS "Should generated files be used to update the cached versions? MODIFIES YOUR SOURCE TREE" OFF)
else()
    set(BUILD_GENERATED_TRAITS OFF)
    set(BUILD_UPDATE_GENERATED_TRAITS OFF)
endif()

set(XRTRAITS_VARIANTS)
if(BUILD_WITH_CXX14)
    list(APPEND XRTRAITS_VARIANTS 14)
endif()
if(BUILD_WITH_CXX17)
    list(APPEND XRTRAITS_VARIANTS 17)
endif()

###
# Decide which, if any, headers we should generate from the registry.
###
set(CACHED_INCLUDE_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/cached-generated")

if(BUILD_GENERATED_TRAITS OR BUILD_GENERATED_OPENXR_HEADERS)
    include(Generate)
endif()

if(BUILD_WITH_EXCEPTIONS)
    xrtraits_standalone_message(STATUS "xrtraits: found fmtlib and building with exceptions.")
else()
    xrtraits_standalone_message(STATUS "xrtraits: not building exceptions-dependent static lib and tests.")
endif()

if(BUILD_GENERATED_OPENXR_HEADERS)
    xrtraits_standalone_message(STATUS "xrtraits: building openxr.h, etc from ${OPENXR_REGISTRY}")
endif()


if(XRTRAITS_IS_STANDALONE_PROJECT)
    include(GNUInstallDirs)
    cmake_dependent_option(BUILD_WITH_CLANG_TIDY "Run the clang-tidy static analysis/lint tool during the build?" OFF
        "CLANGTIDY_COMMAND" OFF)
endif()

add_subdirectory(include)
add_subdirectory(src)

if(XRTRAITS_IS_STANDALONE_PROJECT)
    find_package(Threads REQUIRED)
    include(CTest)
    add_subdirectory(vendor/catch-single-header-2.7.0)
    add_subdirectory(tests)

    include(DoxygenTargets)

    cmake_dependent_option(BUILD_DOC "Add a doc target to build Doxygen documentation?" ON
        "DOXYGEN_FOUND" OFF)
    if(BUILD_DOC)
        configure_file("${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in" "${CMAKE_CURRENT_BINARY_DIR}/Doxyfile" @ONLY)
        add_doxygen("${CMAKE_CURRENT_BINARY_DIR}/Doxyfile"
            OUTPUT_DIRECTORY docs
            NO_PDF
            NO_WARNINGS)
    endif()

    # xrtraits-config-version.cmake used in both build and install trees
    include(CMakePackageConfigHelpers)
    write_basic_package_version_file(
        "${CMAKE_CURRENT_BINARY_DIR}/xrtraits-config-version.cmake"
        VERSION ${PROJECT_VERSION}
        COMPATIBILITY AnyNewerVersion
    )

    set(XRTRAITS_CONFIG_LOCATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME})
    set(XRTRAITS_CONFIG_SRC ${PROJECT_SOURCE_DIR}/cmake/xrtraits-config.cmake)

    ###
    # Config file + friends for build tree
    ###

    # xrtraits-config.cmake
    set(THIS_IS_THE_INPUT_FILE_EDIT_THIS_ONE # Replaces a comment line in xrtraits-config.cmake to distinguish source from generated.
        "This is a generated file! Do not edit! Generated from ${XRTRAITS_CONFIG_SRC}")
    set(XRTRAITS_IS_BUILD_TREE TRUE)
    configure_file(${XRTRAITS_CONFIG_SRC}
        "${CMAKE_CURRENT_BINARY_DIR}/xrtraits-config.cmake"
        @ONLY)

    # xrtraits-config-targets.cmake
    export(EXPORT xrtraits
        FILE "${CMAKE_CURRENT_BINARY_DIR}/xrtraits-config-targets.cmake"
        NAMESPACE xrtraits::)


    # Make this appear from the build tree for a find_package(xrtraits)
    export(PACKAGE xrtraits)


    ###
    # Config file + friends for install tree
    ###

    # xrtraits-config.cmake
    set(XRTRAITS_IS_BUILD_TREE FALSE)
    set(THIS_IS_THE_INPUT_FILE_EDIT_THIS_ONE "This is a generated file! Do not edit!")
    configure_file(${PROJECT_SOURCE_DIR}/cmake/xrtraits-config.cmake
        "${CMAKE_CURRENT_BINARY_DIR}/xrtraits-config-for-install.cmake"
        @ONLY)
    install(FILES
        "${CMAKE_CURRENT_BINARY_DIR}/xrtraits-config-for-install.cmake"
        RENAME xrtraits-config.cmake
        DESTINATION ${XRTRAITS_CONFIG_LOCATION})

    # xrtraits-config-version.cmake and FindOpenXR.cmake (both just copied)
    install(FILES
        ${PROJECT_SOURCE_DIR}/cmake/FindOpenXR.cmake
        "${CMAKE_CURRENT_BINARY_DIR}/xrtraits-config-version.cmake"
        DESTINATION ${XRTRAITS_CONFIG_LOCATION})

    # xrtraits-config-targets.cmake
    install(EXPORT xrtraits
        FILE
        xrtraits-config-targets.cmake
        NAMESPACE
        xrtraits::
        DESTINATION
        ${XRTRAITS_CONFIG_LOCATION})
endif()
