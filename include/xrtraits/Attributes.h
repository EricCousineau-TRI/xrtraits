// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header defining macros corresponding to various C++ attributes
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
// - none

// Library Includes
// - none

// Standard Includes
// - none

#ifdef XRTRAITS_DOXYGEN
//! Compatibility wrapper for `[[fallthrough]]`
#define XRTRAITS_FALLTHROUGH [[fallthrough]]
//! Compatibility wrapper for `[[nodiscard]]`
#define XRTRAITS_NODISCARD [[nodiscard]]
//! Compatibility wrapper for `[[maybe_unused]]`
#define XRTRAITS_MAYBE_UNUSED [[maybe_unused]]
//! Portability wrapper for indicating that a type may alias other types.
#define XRTRAITS_MAY_ALIAS __attribute__((__may_alias__))
#else // XRTRAITS_DOXYGEN

#ifndef __has_cpp_attribute
#define __has_cpp_attribute(X) 0
#endif // !__has_cpp_attribute

#if __has_cpp_attribute(fallthrough)
#define XRTRAITS_FALLTHROUGH [[fallthrough]]
#elif __has_cpp_attribute(clang::fallthrough)
#define XRTRAITS_FALLTHROUGH [[clang::fallthrough]]
#else
#define XRTRAITS_FALLTHROUGH
#endif

#if __has_cpp_attribute(nodiscard)
#define XRTRAITS_NODISCARD [[nodiscard]]
#elif __has_cpp_attribute(gnu::warn_unused_result)
#define XRTRAITS_NODISCARD [[gnu::warn_unused_result]]
#else
#define XRTRAITS_NODISCARD
#endif

#if __has_cpp_attribute(maybe_unused)
#define XRTRAITS_MAYBE_UNUSED [[maybe_unused]]
#elif __has_cpp_attribute(gnu::unused)
#define XRTRAITS_MAYBE_UNUSED [[gnu::unused]]
#else
#define XRTRAITS_MAYBE_UNUSED
#endif

#if defined(__clang__) || defined(__GNUC__)
#define XRTRAITS_MAY_ALIAS __attribute__((__may_alias__))
#else
#define XRTRAITS_MAY_ALIAS
#endif

#endif // XRTRAITS_DOXYGEN
