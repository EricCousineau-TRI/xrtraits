// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once
#include "Common.h"

#include "DynamicVerified.h"

// Needs DynamicVerified and optional
#if defined(XRTRAITS_HAVE_DYNAMIC_VERIFIED) && defined(XRTRAITS_HAVE_OPTIONAL)

/*!
 * Defined after including PolymorphicSpan.h if the compiler and config
 * requirements are satisfied to provide PolymorphicSpan functionality.
 */
#define XRTRAITS_HAVE_POLYMORPHIC_SPAN

// Internal Includes
// - none

// Library Includes
// - none

// Standard Includes
#include <optional>

namespace xrtraits {

using std::nullopt;
using std::optional;

/*! Stores a pointer and a size like gsl::span<T> except here, we **know** that
 * the static type is not actually the dynamic type, so it's not safe to
 * actually index yet.
 */
template <typename T> class PolymorphicSpan;

/*! Similar to gsl::make_span, but the resulting span is only useful for passing
 * to required_verified_span_cast().
 */
template <typename T>
XRTRAITS_NODISCARD inline constexpr PolymorphicSpan<T>
make_polymorphic_span(T* pointer, std::ptrdiff_t size,
                      const char* paramName = nullptr,
                      XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

template <typename T> class PolymorphicSpan
{
public:
	//! Type used for size
	using size_type = std::ptrdiff_t;

	/*! Constructor from a pointer and a size (and optional param name and
	 * error code)
	 */
	constexpr PolymorphicSpan(T* pointer, size_type sz,
	                          const char* paramName = nullptr,
	                          optional<XrResult> const& errorCode = nullopt)
	    : ptr_(pointer), size_(sz), paramName_(paramName),
	      errorCode_(errorCode)
	{}

	//! Get the size of the span
	constexpr size_type size() const { return size_; }

	//! Get the associated data pointer
	constexpr T* data() const { return ptr_; }

	//! Function to perform a cast
	template <typename U>
	constexpr DynamicVerifiedSpan<U>
	cast(const char* paramName = nullptr,
	     optional<XrResult> const& errorCode = nullopt) const
	{
		static_assert(
		    traits::same_constness<T, U>,
		    "Can't change constness with PolymorphicSpan<T>::cast<U>");
		static_assert(traits::has_xr_type_tag_v<U>,
		              "Can only cast with PolymorphicSpan<T>::cast<U> "
		              "to a type with a known tag");
		auto result = errorCode.value_or(
		    errorCode_.value_or(exceptions::DEFAULT_TYPE_ERROR_CODE));
		auto param = (paramName != nullptr) ? paramName : paramName_;
		if (ptr_ == nullptr) {
			throw exceptions::type_error(param, nullptr,
			                             traits::xr_type_tag_v<U>,
			                             size_, result);
		}
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
		return {reinterpret_cast<U*>(ptr_), size_, paramName, result};
	}

private:
	T* ptr_;
	size_type size_;
	const char* paramName_;
	optional<XrResult> errorCode_;
};

/*! Takes a PolymorphicSpan and tries to cast it to a DynamicVerifiedSpan,
 * throwing an exception in the case of dynamic type mismatch.
 *
 * If you don't supply a paramName and errorCode, it will default to the ones
 * passed to make_polymorphic_span()
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedSpan<TargetType>
required_verified_span_cast(PolymorphicSpan<SourceType> const& source,
                            const char* paramName = nullptr,
                            optional<XrResult> const& errorCode = nullopt);
template <typename T>
XRTRAITS_NODISCARD inline constexpr PolymorphicSpan<T>
make_polymorphic_span(T* pointer, std::ptrdiff_t size, const char* paramName,
                      XrResult errorCode)
{
	return {pointer, size, paramName, errorCode};
}

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedSpan<TargetType>
required_verified_span_cast(PolymorphicSpan<SourceType> const& source,
                            const char* paramName,
                            optional<XrResult> const& errorCode)
{
	static_assert(
	    traits::same_constness<SourceType, TargetType>,
	    "Can't change constness with required_verified_span_cast");
	static_assert(traits::has_xr_type_tag_v<TargetType>,
	              "Can only cast with required_verified_span_cast "
	              "to a type with a known tag");
	return source.template cast<TargetType>(paramName, errorCode);
}


#endif // defined(XRTRAITS_HAVE_DYNAMIC_VERIFIED) && defined(XRTRAITS_HAVE_OPTIONAL)
