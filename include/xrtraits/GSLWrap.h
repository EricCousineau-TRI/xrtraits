// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header wrapping or stubbing GSL
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#ifdef XRTRAITS_USE_GSL
#include <gsl/gsl>
#else

#include <assert.h>

// Nearly no-op definitions

/*! When GSL is available, normally tells the optimizer to assume the condition
 * is true.
 */
#ifndef GSL_ASSUME
#define GSL_ASSUME(X) assert(X)
#endif // !GSL_ASSUME

//! When GSL is available, normally asserts a precondition.

#ifndef Expects
#define Expects(X) assert(X)
#endif // !Expects

#endif
