// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header defining unique_ptr-based RAII types for OpenXR handles.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
// - none

// Library Includes
#include <openxr/openxr.h>

// Standard Includes
#include <memory>
#include <type_traits>

namespace xrtraits {

#ifdef XRTRAITS_DOXYGEN
namespace handle_destroyers {
	/*! Placeholder for the generated, per-handle-type deleters for OpenXR
	 * handles.
	 */
	class OpenXRHandleDestroyer;
} // namespace handle_destroyers

/*!
 * @defgroup RAIIHandles RAII Handle Wrappers
 *
 * @brief Types performing RAII-style automatic destruction of handles.
 *
 * These are currently using std::unique_ptr for their implementation, which
 * does mean that they won't work on 32-bit platforms (where handles are 64-bit
 * integers).
 *
 * @todo Make a custom class template to replace the use of std::unique_ptr for
 * 32-bit compatibility.
 */
//! @{

//! Scoped RAII pointer type wrapping XrInstance
using UniqueInstance =
    std::unique_ptr<std::remove_pointer_t<XrInstance>,
                    handle_destroyers::OpenXRHandleDestroyer>;

//! Scoped RAII pointer type wrapping XrSession
using UniqueSession = std::unique_ptr<std::remove_pointer_t<XrSession>,
                                      handle_destroyers::OpenXRHandleDestroyer>;

//! Scoped RAII pointer type wrapping XrSwapchain
using UniqueSwapchain =
    std::unique_ptr<std::remove_pointer_t<XrSwapchain>,
                    handle_destroyers::OpenXRHandleDestroyer>;

//! Scoped RAII pointer type wrapping XrAction
using UniqueAction = std::unique_ptr<std::remove_pointer_t<XrAction>,
                                     handle_destroyers::OpenXRHandleDestroyer>;

//! Scoped RAII pointer type wrapping XrActionSet
using UniqueActionSet =
    std::unique_ptr<std::remove_pointer_t<XrActionSet>,
                    handle_destroyers::OpenXRHandleDestroyer>;

//! Scoped RAII pointer type wrapping XrSpace
using UniqueSpace = std::unique_ptr<std::remove_pointer_t<XrSpace>,
                                    handle_destroyers::OpenXRHandleDestroyer>;
//! @}
#else

#define XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(HandleName)                         \
	namespace handle_destroyers {                                          \
		class Destroy##HandleName                                      \
		{                                                              \
		public:                                                        \
			Destroy##HandleName() = default;                       \
			void operator()(Xr##HandleName handle) const           \
			{                                                      \
				if (handle != XR_NULL_HANDLE) {                \
					xrDestroy##HandleName(handle);         \
				}                                              \
			}                                                      \
		};                                                             \
	}                                                                      \
	using Unique##HandleName =                                             \
	    std::unique_ptr<std::remove_pointer_t<Xr##HandleName>,             \
	                    handle_destroyers::Destroy##HandleName>

XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(Instance);
XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(Session);
XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(Swapchain);
XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(Action);
XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(ActionSet);
XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE(Space);

#undef XRTRAITS_CREATE_UNIQUE_HANDLE_TYPE

#endif // XRTRAITS_DOXYGEN

} // namespace xrtraits
