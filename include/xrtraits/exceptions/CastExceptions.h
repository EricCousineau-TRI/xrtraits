// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#ifdef XRTRAITS_USE_EXCEPTIONS
// Internal Includes
#include "BaseExceptions.h" // IWYU pragma: export

// Library Includes
// - none

// Standard Includes
#include <cstddef> // for std::nullptr_t

namespace xrtraits {
namespace exceptions {

	/*!
	 * Exception thrown when we can't perform a given tagged cast to
	 * a reference type due to type field mismatch or null input.
	 *
	 * Only thrown on xr_tagged_dynamic_cast or xr_tagged_risky_cast
	 * to a reference type, because a reference is logically never
	 * null so a mismatch can't be handled by returning nullptr.
	 * Input of a nullptr gets a slightly different what() message
	 * but is the same exception as thrown when the input is a
	 * reference to or a non-null pointer to an object with a
	 * non-matching `type` member.
	 *
	 * @ingroup exceptions
	 */
	struct bad_tagged_cast : xr_runtime_error
	{
		/*!
		 * Constructor used for a tag mismatch with non-null
		 * input.
		 */
		bad_tagged_cast(XrStructureType destTag,
		                XrStructureType sourceTag,
		                const char* castType);

		/*!
		 * Constructor used for nullptr input.
		 */
		bad_tagged_cast(XrStructureType destTag, std::nullptr_t,
		                const char* castType);
	};

} // namespace exceptions
} // namespace xrtraits

#endif // XRTRAITS_USE_EXCEPTIONS
