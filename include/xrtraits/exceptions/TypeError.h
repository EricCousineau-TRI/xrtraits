// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#ifdef XRTRAITS_USE_EXCEPTIONS

// Internal Includes
#include "BaseExceptions.h" // IWYU pragma: export

// Library Includes
// - none

// Standard Includes
#include <cstddef> // for std::nullptr_t

namespace xrtraits {
namespace exceptions {

	/*!
	 * Default error code for type errors.
	 *
	 * @relates type_error
	 */
	static constexpr XrResult DEFAULT_TYPE_ERROR_CODE =
	    XR_ERROR_VALIDATION_FAILURE;

	/*!
	 * Exception thrown by xrtraits::verifyDynamicType() and friends when
	 * input isn't the type required.
	 *
	 * @ingroup exceptions
	 */
	struct type_error : xr_logic_error
	{
		/*!
		 * Constructor taking a message
		 */
		type_error(std::string&& msg, XrResult result);

		/*!
		 * Constructor for getting nullptr instead of the given type as
		 * a parameter
		 */
		type_error(const char* paramName, std::nullptr_t,
		           XrStructureType expectedType, XrResult result);

		/*!
		 * Constructor for getting something other than the expected
		 * type as a parameter
		 */
		type_error(const char* paramName, XrStructureType actualType,
		           XrStructureType expectedType, XrResult result);

		/*! Constructor for getting something other than the expected
		 * type as an array element.
		 */
		type_error(const char* paramName, XrStructureType actualType,
		           XrStructureType expectedType, std::ptrdiff_t index,
		           XrResult result);

		/*! Constructor for getting nullptr instead of a pointer to an
		 * array of the given size and type.
		 */
		type_error(const char* paramName, std::nullptr_t,
		           XrStructureType expectedType, std::ptrdiff_t size,
		           XrResult result);
	};

	/*!
	 * Exception thrown by xrtraits::getFromChain() and friends when input
	 * doesn't include the type required.
	 *
	 * @ingroup exceptions
	 */
	struct get_from_chain_error : type_error
	{
		/*!
		 * Constructor taking a message.
		 */
		get_from_chain_error(std::string&& msg, XrResult result);

		/*!
		 * Constructor for getting nullptr instead of a chain containing
		 * a desired type.
		 */
		get_from_chain_error(const char* paramName, std::nullptr_t,
		                     XrStructureType expectedType,
		                     XrResult result);

		/*!
		 * Constructor for not finding the desired type in a struct
		 * chain.
		 */
		get_from_chain_error(const char* paramName,
		                     XrStructureType headType,
		                     XrStructureType expectedType,
		                     XrResult result);
	};
} // namespace exceptions
} // namespace xrtraits

#endif // XRTRAITS_USE_EXCEPTIONS
