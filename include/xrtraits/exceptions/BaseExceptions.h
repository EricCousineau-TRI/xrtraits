// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#ifdef XRTRAITS_USE_EXCEPTIONS

// Internal Includes
// - none

// Library Includes
#include <openxr/openxr.h>

// Standard Includes
#include <exception> // IWYU pragma: export
#include <string>

namespace xrtraits {
namespace exceptions {

	/*!
	 * Base class for all exceptions explicitly thrown by this
	 * implementation's code.
	 *
	 * One constructor permits association of a suggested XrResult code,
	 * used in XRTRAITS_CATCH_FALLBACK.
	 *
	 * @note Do not instantiate directly - use exceptions::xr_logic_error or
	 * exceptions::xr_runtime_error, or a more specific exception if one
	 * exists.
	 *
	 * @ingroup exceptions
	 */
	class xr_exception : std::exception
	{
	public:
		/*!
		 * A human-readable string describing the exception.
		 */
		const char* what() const noexcept override;

		/*!
		 * @return true if an XrResult code was passed when creating
		 * this exception.
		 */
		bool hasResult() const noexcept;

		/*!
		 * Access the suggested/associated XrResult.
		 * If none was supplied at creation, it returns the default,
		 * XR_ERROR_RUNTIME_FAILURE.
		 */
		XrResult result() const noexcept;

	protected:
		/*!
		 * Protected constructor (without a result code) for use by
		 * derived classes.
		 */
		xr_exception(std::string&& what);

		/*!
		 * Protected constructor (with a result code) for use by derived
		 * classes.
		 */
		xr_exception(std::string const& what, XrResult err);

		/*!
		 * Protected constructor (with a result code) for use by derived
		 * classes.
		 */
		xr_exception(std::string&& what, XrResult err);

	private:
		std::string what_;
		XrResult result_ = XR_ERROR_RUNTIME_FAILURE;
		bool hasResult_ = false;
	};

	/*!
	 * Base class for OpenXR-related exceptions explicitly thrown by this
	 * library's code.
	 *
	 * Conceptually related to std::runtime_error, though not deriving from
	 * it. May specify an associated/recommended XrResult code.
	 *
	 * @note Prefer instantiating derived classes, if a suitable one exists,
	 * instead of instantiating this class directly.
	 *
	 * @ingroup exceptions
	 */
	struct xr_runtime_error : xr_exception
	{
	public:
		/*!
		 * Constructor taking a "what" string as well as an XrResult.
		 */
		xr_runtime_error(std::string&& what, XrResult err);

		/*!
		 * Constructor taking a "what" string as well as an XrResult.
		 */
		xr_runtime_error(std::string const& what, XrResult err);

		/*!
		 * Constructor taking a "what" string
		 */
		xr_runtime_error(std::string&& what);

		/*!
		 * Constructor taking an XrResult (which will be included in the
		 * "what" string), a condition, and a message.
		 *
		 * Primarily used by throwIfNotSucceeded() and
		 * throwIfNotUnqualifiedSuccess()
		 */
		xr_runtime_error(XrResult err, const char* condition,
		                 const char* msg);
	};

	/*!
	 * Throw an xr_runtime_error with the given message if the result fails
	 * XR_SUCCEEDED.
	 *
	 * Particularly useful for application-side utilities to allow
	 * non-XrResult return values in the usual case.
	 *
	 * @see throwIfNotUnqualifiedSuccess
	 */
	void throwIfNotSucceeded(XrResult result, const char* errorMessage);

	/*!
	 * Throw an xr_runtime_error with the given message if the result fails
	 * XR_UNQUALIFIED_SUCCESS.
	 *
	 * That is, if result is not exactly XR_SUCCESS.
	 *
	 * Particularly useful for application-side utilities to allow
	 * non-XrResult return values in the usual case.
	 *
	 * @see throwIfNotUnqualifiedSuccess
	 */
	void throwIfNotUnqualifiedSuccess(XrResult result,
	                                  const char* errorMessage);

	/*!
	 * Base class for "logic" exceptions explicitly thrown by this library's
	 * code.
	 *
	 * Conceptually related to std::logic_error, though not deriving from
	 * it. May specify an associated/recommended XrResult code.
	 *
	 * @note Prefer instantiating derived classes, instead of instantiating
	 * this class directly.
	 *
	 * @ingroup exceptions
	 */
	struct xr_logic_error : xr_exception
	{
	public:
		/*!
		 * Constructor taking a "what" string as well as an XrResult.
		 */
		xr_logic_error(std::string&& what, XrResult err);

		/*!
		 * Constructor taking a "what" string as well as an XrResult.
		 */
		xr_logic_error(std::string const& what, XrResult err);

		/*!
		 * Constructor taking a "what" string.
		 */
		xr_logic_error(std::string&& what);
	};

} // namespace exceptions
} // namespace xrtraits

#endif // XRTRAITS_USE_EXCEPTIONS
