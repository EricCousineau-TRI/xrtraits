// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing wrappers for returning a variable-length collection
 * by repeatedly calling a "two-call idiom" OpenXR function for you. Lets you
 * pretend it's only a single call, possibly returning a std::vector<> (for some
 * variants).
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "InitXrType.h"

#ifdef XRTRAITS_USE_EXCEPTIONS
#include "exceptions/BaseExceptions.h"
#endif

// Library Includes
// - none

// Standard Includes
// - none

namespace xrtraits {

/*!
 * @defgroup TwoCall Two-Call Idiom Wrappers
 *
 * @brief Functions simplifying calls that return a variable-length
 * buffer/array.
 *
 * Some of these functions may take a leading argument, such as a size hint or a
 * container to populate. However, after that possible argument, the rest are
 * standardized:
 *
 * - wrappedCall is something callable (lambda or function). It will receive
 *   `capacityInput`, `countOutput`, and `array` parameters as its only (if no
 *   more arguments are passed to the function) or last (if additional
 *   parameters are passed) parameters.
 * - Any additional arguments passed will be forwarded to the call
 *   **before** the `capacityInput`, `countOutput`, and `array` parameters.
 */

/*!
 * @example twocall-just-function.cpp
 *
 * This example shows how to use doTwoCall() when a function requires no
 * additional arguments.
 */

/*!
 * @example twocall-function-and-one-arg.cpp
 *
 * This example shows how to use doTwoCall() when a function requires one
 * additional argument.
 *
 * The wrapper will make a call like the following as required to retrieve all
 * properties (adjusting the parameters and array between calls):
 *
 * ```c++
 * XrResult result = xrEnumerateInstanceExtensionProperties(nullptr,
 *     capacityInput, &countOutput, array);
 * ```
 *
 * Note how the one additional argument, `nullptr`, has been forwarded to the
 * call before the capacity, count, and array args.
 */

/*!
 * @example twocall-size-hint-and-initialized.cpp
 *
 * This example shows a typical usage of doTwoCallWithSizeHint(): when locating
 * views. The number of views should in theory be known, but for safety and
 * consistency this function is specified in two-call style.
 *
 * It also shows a few uses of xrtraits::Initialized - one (`viewState`) with no
 * arguments (which sets just the `type` and `next`), and one (`viewLocateInfo`)
 * with some arguments that aren't a chained struct. In this case, `type` and
 * `next` are initialized like normal, while the subsequent members of
 * XrViewLocateInfo, `displayTime` and `space`, are initialized from `frameTime`
 * and `local` respectively.
 *
 * Because a size hint was provided, this call will initially create a vector of
 * `numViews` `XrView` structures before attempting to call xrLocateViews. This
 * will generally reduce the number of calls to xrLocateViews to 1, since the
 * initial capacity should be sufficient. The wrapper will make a call like the
 * following as required to retrieve all properties (adjusting the parameters
 * and array between calls):
 *
 * ```c++
 * XrResult result = xrLocateViews(session, &viewLocateInfo, &viewState,
 *     capacityInput, &countOutput, array);
 * ```
 *
 * Note how the several additional arguments were forwarded to the
 * call before the capacity, count, and array args.
 */

/*!
 * @example twocall-in-place-sized-and-initialized.cpp
 *
 * This example shows a mostly-equivalent scenario to @ref
 * twocall-size-hint-and-initialized.cpp, but instead uses the
 * doTwoCallInPlace() function which avoids exceptions and returns the XrResult
 * from the final wrapped call. See that other example for more detail,
 * including explanations of the xrtraits::Initialized usage.
 *
 * Because `views` is not empty, that will be used as a size hint, so if (as
 * expected) `xrLocateViews` output fits in that size, only a single call to the
 * wrapped function is needed.
 */

/*!
 * @example twocall-in-place-just-function.cpp
 *
 * This example shows a mostly-equivalent scenario to @ref
 * twocall-just-function.cpp, but instead uses the doTwoCallInPlace() function
 * which avoids exceptions and returns the XrResult from the final wrapped call.
 * See that other example for more detail overall.
 *
 * Because `views` is empty, the normal two-call process will take place: A call
 * with 0 capacity will be made first, the vector will be resized (with its
 * elements initialized appropriately). After this capacity check, another call
 * to the wrapped function will be made, now with the current capacity and
 * array, and will hopefully be successful.
 */

#ifndef XRTRAITS_DOXYGEN
namespace detail {

	static constexpr uint32_t MAX_CALLS_FOR_TWO_CALL_IDIOM = 5;
	struct TwoCallResult
	{
		bool doneCalling = false;
		XrResult returnCode = XR_SUCCESS;
		//! Only valid if returnCode is XR_SUCCESS or XR_ERROR_
		uint32_t count = 0;
	};
	template <typename F, typename... Args>
	static inline TwoCallResult getCount(F&& wrappedCall, Args&&... a)
	{
		TwoCallResult ret;
		ret.returnCode = wrappedCall(std::forward<Args>(a)..., 0,
		                             &ret.count, nullptr);

		if (ret.returnCode != XR_SUCCESS) {
			// Zero should always give success, whether there are 0
			// items or more.
			ret.doneCalling = true;
		} else if (ret.count == 0) {
			// We asked for the count, and it was zero, so all done.
			ret.doneCalling = true;
		}
		return ret;
	}

	template <typename T, typename F, typename... Args>
	static inline TwoCallResult callOnce(std::vector<T>& container,
	                                     F&& wrappedCall, Args&&... a)
	{
		using namespace xrtraits;

		TwoCallResult ret;
		if (container.empty()) {
			// No capacity, just treat as a count retrieval.
			ret = getCount(std::forward<F>(wrappedCall),
			               std::forward<Args>(a)...);
		} else {
			// We have at least some capacity already.
			ret.returnCode =
			    wrappedCall(std::forward<Args>(a)...,
			                uint32_t(container.size()), &ret.count,
			                container.data());

			// If we get a non-size related error, or a success,
			// we're done.
			ret.doneCalling =
			    ret.returnCode != XR_ERROR_SIZE_INSUFFICIENT;
		}
		// Resize accordingly
		if (ret.returnCode == XR_SUCCESS ||
		    ret.returnCode == XR_ERROR_SIZE_INSUFFICIENT) {
			container.resize(ret.count, make_zeroed<T>());
		}

		return ret;
	}

	template <typename T, typename F, typename... Args>
	static inline XrResult twoCallLoop(uint32_t max_calls,
	                                   std::vector<T>& container,
	                                   F&& wrappedCall, Args&&... a)
	{
		TwoCallResult result;
		// Repeatedly call until we succeed, fail, or get bored of
		// resizing.
		for (uint32_t i = 0; !result.doneCalling && i < max_calls;
		     ++i) {
			result =
			    callOnce(container, std::forward<F>(wrappedCall),
			             std::forward<Args>(a)...);
		}
		return result.returnCode;
	}
} // namespace detail

#endif // !XRTRAITS_DOXYGEN

#ifdef XRTRAITS_USE_EXCEPTIONS
/*! Perform the two call idiom, returning a vector.
 *
 * @tparam T The type of the buffer element you expect
 * @param wrappedCall A function or lambda that takes the `capacityInput`,
 * `countOutput`, and `array` parameters as its only or last parameters.
 * @param a Any additional arguments passed to this call will be forwarded to
 * the call **before** the `capacityInput`, `countOutput`, and `array`
 * parameters.
 *
 * @throws if final call does not return XR_SUCCESS
 *
 * Requires XR_USE_EXCEPTIONS.
 *
 * @ingroup TwoCall
 */
template <typename T, typename F, typename... Args>
inline std::vector<T> doTwoCall(F&& wrappedCall, Args&&... a)
{
	using xrtraits::make_zeroed_vector;
	auto countResults = detail::getCount(std::forward<F>(wrappedCall),
	                                     std::forward<Args>(a)...);

	exceptions::throwIfNotUnqualifiedSuccess(
	    countResults.returnCode, "Failed getting count in two-call idiom");

	std::vector<T> container = make_zeroed_vector<T>(countResults.count);
	if (!container.empty()) {
		XrResult result = detail::twoCallLoop(
		    detail::MAX_CALLS_FOR_TWO_CALL_IDIOM, container,
		    std::forward<F>(wrappedCall), std::forward<Args>(a)...);
		exceptions::throwIfNotUnqualifiedSuccess(
		    result, "Failed in retrieving two-call values");
	}

	return container;
}

/*! Perform the two call idiom, returning a vector, when we already have a hint
 * about the capacity.
 *
 * @tparam T The type of the buffer element you expect
 * @param sizeHint The buffer size to attempt: if sufficient, only one call to
 * the wrappedCall will be made.
 * @param wrappedCall A function or lambda that takes the `capacityInput`,
 * `countOutput`, and `array` parameters as its only or last parameters.
 * @param a Any additional arguments passed to this call will be forwarded to
 * the call **before** the `capacityInput`, `countOutput`, and `array`
 * parameters.
 *
 * @throws if final call does not return XR_SUCCESS
 *
 * Requires XR_USE_EXCEPTIONS.
 *
 * @ingroup TwoCall
 */
template <typename T, typename F, typename... Args>
inline std::vector<T> doTwoCallWithSizeHint(uint32_t sizeHint, F&& wrappedCall,
                                            Args&&... a)
{
	using xrtraits::make_zeroed_vector;
	std::vector<T> container = make_zeroed_vector<T>(sizeHint);
	XrResult result = detail::twoCallLoop(
	    detail::MAX_CALLS_FOR_TWO_CALL_IDIOM, container,
	    std::forward<F>(wrappedCall), std::forward<Args>(a)...);
	exceptions::throwIfNotUnqualifiedSuccess(
	    result, "Failed in retrieving two-call values");
	return container;
}
#endif // XRTRAITS_USE_EXCEPTIONS

/*! Perform the two call idiom, returning XrResult, to populate an existing
 * container, whose size may hint at expected count.
 *
 * @param container The container to fill. If it is not empty, the buffer size
 * will be used as a size hint: if sufficient, only one call to the wrappedCall
 * will be made.
 * @param wrappedCall A function or lambda that takes the `capacityInput`,
 * `countOutput`, and `array` parameters as its only or last parameters.
 * @param a Any additional arguments passed to this call will be forwarded to
 * the call **before** the `capacityInput`, `countOutput`, and `array`
 * parameters.
 *
 * Does not require XR_USE_EXCEPTIONS.
 *
 * @ingroup TwoCall
 */
template <typename T, typename F, typename... Args>
inline XrResult doTwoCallInPlace(std::vector<T>& container, F&& wrappedCall,
                                 Args&&... a)
{

	return detail::twoCallLoop(detail::MAX_CALLS_FOR_TWO_CALL_IDIOM,
	                           container, std::forward<F>(wrappedCall),
	                           std::forward<Args>(a)...);
}

} // namespace xrtraits
