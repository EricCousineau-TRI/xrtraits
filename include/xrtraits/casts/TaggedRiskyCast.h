// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing a slightly-riskier relative of
 * xr_tagged_dynamic_cast() that works on void pointers (as long as they
 * actually do point to a tagged type), called xr_tagged_risky_cast().
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "../Common.h"

#ifdef XRTRAITS_HAVE_CONSTEXPR_IF

// Internal Includes
#include "../Attributes.h"
#include "../exceptions/CastExceptions.h"
#include "TaggedCastCommon.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace xrtraits {

namespace casts {
#ifndef XRTRAITS_DOXYGEN
	namespace detail {
		using ::std::conditional_t;
		using ::std::enable_if_t;
		using ::std::is_const;
		using ::std::is_pointer;
		using ::std::is_reference;
		using ::std::is_same;
		using ::std::remove_pointer_t;

		/*! Cast to XrBaseInStructure const* or XrBaserOutStructure*
		 * depending on the constness of the input.
		 */
		template <typename SourceType>
		inline auto cast_to_base(SourceType source)
		{
			static_assert(
			    is_same_v<SourceType, const void*> ||
			        is_same_v<SourceType, void*>,
			    "cast_to_base is only for void* or const void*");

			if constexpr (points_to_const_v<SourceType>) {
				// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
				return reinterpret_cast<
				    XrBaseInStructure const*>(source);
			} else { // NOLINT(readability-else-after-return)
				// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
				return reinterpret_cast<XrBaseOutStructure*>(
				    source);
			}
		}
		/*! Given a non-null const void * or void * pointing to an
		 * OpenXR tagged type, retrieve the value of its type
		 * member.
		 */
		template <typename SourceType>
		inline XrStructureType risky_get_type(SourceType source)
		{
			static_assert(
			    is_same_v<SourceType, const void*> ||
			        is_same_v<SourceType, void*>,
			    "risky_get_type is only for void* or const void*");
			return cast_to_base(source)->type;
		}
	} // namespace detail

#endif // !XRTRAITS_DOXYGEN

	/*! OpenXR structure cast, somewhat analogous to a slightly-riskier
	 * version of the standard dynamic_cast.
	 *
	 * Cast a `void *` (or `void const *`) that is assumed to be a pointer
	 * to some XR tagged type, to a given XR tagged type pointer, if the tag
	 * matches. Provide a single type parameter (target type) as well as the
	 * input pointer.
	 *
	 * Undefined behavior if the `void *` is not a pointer to some XR tagged
	 * type, because we behave as if it were! If you have an actual
	 * specific type, not just a tag, then see xr_tagged_dynamic_cast
	 * pointer/reference parameter.
	 *
	 * Input should be a pointer or reference to a (possibly const)
	 * structure, with static type starting with `XrStructureType type;
	 * const void * next;`. The `type` member of the input is examined to
	 * ensure it matches the value defined by the spec (as indicated by
	 * generated type traits) for the TargetType provided.
	 *
	 * - If TargetType is a pointer type, then a mismatch of `type` (or
	 * null input) results in a nullptr return.
	 * - If TargetType is a reference type, then a mismatch of `type` (or
	 * null input) results in a bad_tagged_cast exception.
	 *
	 * Unlike xr_tagged_dynamic_cast, this function cannot take references
	 * as input.
	 *
	 * @ingroup Casts
	 */
	template <typename TargetType, typename SourceType>
	inline TargetType xr_tagged_risky_cast(SourceType source)
	{
		using Tgt = TargetType;
		// This cast operation assumes that if you're calling
		// it, you know that the source parameter type is an XR
		// tagged type
		static_assert(
		    is_same_v<detail::remove_reference_pointer_cv_t<SourceType>,
		              void>,
		    "Can only use xr_tagged_risky_cast to cast from a "
		    "pointer to (possibly const) void. If you have something "
		    "with real type data, you are looking for "
		    "xr_tagged_dynamic_cast.");

		static_assert(
		    traits::is_xr_tagged_type_v<Tgt>,
		    "Can only use xr_tagged_risky_cast to cast to a "
		    "pointer/reference to a (possibly const) XR tagged type.");
		// Cast determines whether to perform reinterpret or
		// return nullptr based on comparing value of `type` to
		// expected value for the target type, so there must
		// actually be such an expected value. (Note that we
		// don't require this for the source type: there we only
		// care that the member exists, so we can compare it to
		// the value we're verifying the existence of here.)
		static_assert(traits::has_xr_type_tag_v<Tgt>,
		              "Can only use xr_tagged_risky_cast to cast to a "
		              "pointer/reference to a concrete XR tagged type "
		              "with a known, defined type tag value "
		              "(XrStructureType enum value).");

		static_assert(detail::points_or_refers_to_const_v<SourceType>
		                  ? detail::points_or_refers_to_const_v<Tgt>
		                  : true,
		              "Cannot remove const qualification through "
		              "xr_tagged_risky_cast");

#ifndef XRTRAITS_USE_EXCEPTIONS
		static_assert(is_pointer_v<Tgt>,
		              "Casts to references require the availability of "
		              "xrtraits exceptions.");
#endif // !XRTRAITS_USE_EXCEPTIONS

		XRTRAITS_MAYBE_UNUSED static const char castType[] =
		    "xr_tagged_dynamic_cast";
		constexpr bool is_target_pointer = is_pointer_v<Tgt>;

		// Always pointer input: must check for nullptr.
		if (source == nullptr) {
			if constexpr (is_target_pointer) {
				// nullptr in, return nullptr
				return nullptr;
			} else { // NOLINT(readability-else-after-return)

				// nullptr in, exception
#ifdef XRTRAITS_USE_EXCEPTIONS
				throw exceptions::bad_tagged_cast(
				    traits::xr_type_tag_v<Tgt>, nullptr,
				    castType);
#endif // XRTRAITS_USE_EXCEPTIONS
			}
		}

		if (traits::xr_type_tag_v<Tgt> ==
		    detail::risky_get_type(source)) {
			// matching type tag in: do the casting and
			// address-taking/dereferencing
			return detail::perform_dereferencing_reinterpret_cast<
			    Tgt>(source);
		}

		// tag on type didn't match what was expected
		if constexpr (is_target_pointer) {
			// tag mismatch, return nullptr
			return nullptr;
		} else { // NOLINT(readability-else-after-return)

			// tag mismatch, throw exception
#ifdef XRTRAITS_USE_EXCEPTIONS
			throw exceptions::bad_tagged_cast(
			    traits::xr_type_tag_v<Tgt>,
			    detail::risky_get_type(source), castType);
#endif // XRTRAITS_USE_EXCEPTIONS
		}
	}

} // namespace casts

} // namespace xrtraits

#endif // XRTRAITS_HAVE_CONSTEXPR_IF
