// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing a dynamic_cast equivalent for OpenXR tagged types,
 * xr_tagged_dynamic_cast().
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "../Common.h"

#ifdef XRTRAITS_HAVE_CONSTEXPR_IF

// Internal Includes
#include "../Attributes.h"
#include "../exceptions/CastExceptions.h"
#include "TaggedCastCommon.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace xrtraits {

namespace casts {

	/*! OpenXR structure cast, analogous to the standard
	 * dynamic_cast.
	 *
	 * Provide a single type parameter (target type) as well as the
	 * pointer/reference parameter.
	 *
	 * Input should be a pointer or reference to a (possibly const)
	 * structure, with static type starting with `XrStructureType
	 * type; const void * next;`. The `type` member of the input is
	 * examined to ensure it matches the value defined by the spec
	 * (as indicated by generated type traits) for the TargetType
	 * provided.
	 *
	 * - If TargetType is a pointer type, then a mismatch of `type`
	 * (or input of nullptr) results in a nullptr return.
	 * - If TargetType is a reference type, then a mismatch of
	 * `type` (or input of nullptr) results in a bad_tagged_cast
	 * exception.
	 *
	 * @ingroup Casts
	 */
	template <typename TargetType, typename SourceType>
	inline TargetType xr_tagged_dynamic_cast(SourceType&& source)
	{
		using std::conditional_t;
		using std::remove_reference_t;
		using Src =
		    conditional_t<is_pointer_v<remove_reference_t<SourceType>>,
		                  remove_reference_t<SourceType>, SourceType>;
		using Tgt = TargetType;
		// This cast operation requires knowing that the
		// parameter type is an XR tagged type: it doesn't just
		// take your word for it like xr_tagged_risky_cast does.
		static_assert(
		    traits::is_xr_tagged_type_v<Src>,
		    "Can only use xr_tagged_dynamic_cast to cast from "
		    "a pointer/reference to (possibly const) XR tagged "
		    "types. If you really want to cast from something like a "
		    "void *, you are looking for xr_tagged_risky_cast.");

		static_assert(
		    traits::is_xr_tagged_type_v<Tgt>,
		    "Can only use xr_tagged_dynamic_cast to cast to a "
		    "pointer/reference to a (possibly const) XR tagged "
		    "type.");

		// Cast determines whether to perform reinterpret or
		// return nullptr based on comparing value of `type` to
		// expected value for the target type, so there must
		// actually be such an expected value. (Note that we
		// don't require this for the source type: there we only
		// care that the member exists, so we can compare it to
		// the value we're verifying the existence of here.)
		static_assert(
		    traits::has_xr_type_tag_v<Tgt>,
		    "Can only use xr_tagged_dynamic_cast to cast to a "
		    "pointer/reference to a concrete XR tagged type "
		    "with a known, defined type tag value (XrStructureType "
		    "enum value).");

		static_assert(detail::points_or_refers_to_const_v<Src>
		                  ? detail::points_or_refers_to_const_v<Tgt>
		                  : true,
		              "Cannot remove const qualification through "
		              "xr_tagged_dynamic_cast");
#ifndef XRTRAITS_USE_EXCEPTIONS
		static_assert(is_pointer_v<Tgt>,
		              "Casts to references require the availability of "
		              "xrtraits exceptions.");
#endif // !XRTRAITS_USE_EXCEPTIONS

		XRTRAITS_MAYBE_UNUSED static const char castType[] =
		    "xr_tagged_dynamic_cast";
		constexpr bool is_target_pointer = is_pointer_v<Tgt>;

		if constexpr (is_pointer_v<Src>) {
			// Pointer input: must check for nullptr.

			if (source == nullptr) {
				if constexpr (is_target_pointer) {
					// nullptr in, return nullptr
					return nullptr;
				} else { // NOLINT(readability-else-after-return)

					// nullptr in, exception
#ifdef XRTRAITS_USE_EXCEPTIONS
					throw exceptions::bad_tagged_cast(
					    traits::xr_type_tag_v<Tgt>, nullptr,
					    castType);
#endif // XRTRAITS_USE_EXCEPTIONS
				}
			}
		}

		if (traits::xr_type_tag_v<Tgt> == detail::get_type(source)) {
			// matching type tag in: do the casting and
			// address-taking/dereferencing
			return detail::perform_dereferencing_reinterpret_cast<
			    Tgt>(source);
		}

		// tag on type didn't match what was expected
		if constexpr (is_target_pointer) {
			// tag mismatch, return nullptr
			return nullptr;
		} else { // NOLINT(readability-else-after-return)

			// tag mismatch, throw exception
#ifdef XRTRAITS_USE_EXCEPTIONS
			throw exceptions::bad_tagged_cast(
			    traits::xr_type_tag_v<Tgt>,
			    detail::get_type(source), castType);
#endif // XRTRAITS_USE_EXCEPTIONS
		}
	}

} // namespace casts

} // namespace xrtraits

#endif // XRTRAITS_HAVE_CONSTEXPR_IF
