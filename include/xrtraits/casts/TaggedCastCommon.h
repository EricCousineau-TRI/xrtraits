// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header shared between the two type-safe cast headers.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// IWYU pragma: private
// IWYU pragma: friend "Tagged.*Cast\.h"

#include "../Common.h"

#ifdef XRTRAITS_HAVE_CONSTEXPR_IF

/*!
 * Defined after including any of the cast headers or GetChained.h if the compiler and config
 * requirements are satisfied to provide at least some cast/get-chained functionality.
 *
 * Without exceptions, casts to reference types are unavailable.
 */
#define XRTRAITS_HAVE_CASTS

// Internal Includes
#include "../Common.h"
#include "../traits/APITraits.h"

// Library Includes
#include <openxr/openxr.h>

// Standard Includes
#include <type_traits>

namespace xrtraits {
namespace casts {

#ifndef XRTRAITS_DOXYGEN
	namespace detail {

		using ::std::add_pointer_t;
		using ::std::enable_if_t;
		using ::std::integral_constant;
		using ::std::is_const;
		using ::std::is_pointer;
		using ::std::remove_const_t;
		using ::std::remove_pointer_t;
		using namespace ::xrtraits::traits::detail;

		/*! Alias to shorten finding constness of pointed/referred to
		 * object.
		 */
		template <typename T>
		constexpr bool points_or_refers_to_const_v =
		    is_const_v<detail::remove_reference_pointer_t<T>>;

		/*! Alias to simplify conditional for selecting
		 * perform_pointer_reinterpret_cast implementation path
		 */
		template <typename T>
		constexpr bool points_to_const_v =
		    is_const_v<remove_pointer_t<T>>;
		/*! Alias to simplify conditional for selecting
		 * perform_pointer_reinterpret_cast implementation path
		 */
		template <typename T>
		constexpr bool is_pointer_to_const_v =
		    is_pointer_v<T>&& points_to_const_v<T>;
		/*! Alias to simplify conditional for selecting
		 * perform_pointer_reinterpret_cast implementation path
		 */
		template <typename T>
		constexpr bool
		    is_pointer_to_nonconst_v = is_pointer_v<T> &&
		                               (!points_to_const_v<T>);

		/*! Alias to simplify conditional for selecting
		 * perform_pointer_reinterpret_cast implementation path
		 */
		template <typename T, typename U>
		constexpr bool pointed_to_constness_is_equal_v =
		    points_to_const_v<T> == points_to_const_v<U>;
		/*! Reinterpret a pointer, optionally adding (but never
		 * removing) const qualification to the pointed-to type.
		 */
		template <typename TargetType, typename SourceType>
		inline TargetType
		perform_pointer_reinterpret_cast(SourceType* source)
		{
			using Source = add_pointer_t<SourceType>;
			static_assert(!(is_pointer_to_const_v<
			                    add_pointer_t<SourceType>> &&
			                is_pointer_to_nonconst_v<TargetType>),
			              "Can't remove constness with "
			              "perform_pointer_reinterpret_cast");
			if constexpr (pointed_to_constness_is_equal_v<
			                  SourceType, TargetType>) {
				return reinterpret_cast<TargetType>( // NOLINT
				    source);

			} else { // NOLINT
				static_assert(
				    is_pointer_to_nonconst_v<Source> &&
				        is_pointer_to_const_v<TargetType>,
				    "If constness doesn't match, must have "
				    "ptr-to-non-const source and ptr-to-const "
				    "target.");
				using TargetNonConst =
				    add_pointer_t<remove_const_t<
				        remove_pointer_t<TargetType>>>;
				auto t =
				    reinterpret_cast<TargetNonConst>( // NOLINT
				        source);

				return const_cast<TargetType>(t); // NOLINT
			}
		}

		template <typename T> inline auto get_pointer(T&& obj)
		{
			if constexpr (is_pointer_v<remove_reference_t<T>>) {
				return obj;
			} else { // NOLINT(readability-else-after-return)
				return &obj;
			}
		}

		template <typename TargetType, typename SourceType>
		inline TargetType
		perform_dereferencing_reinterpret_cast(SourceType&& source)
		{
			constexpr bool isTargetPtr = is_pointer_v<TargetType>;

			auto sourcePtr = get_pointer(source);
			if constexpr (isTargetPtr) {
				return perform_pointer_reinterpret_cast<
				    TargetType>(sourcePtr);
			} else { // NOLINT(readability-else-after-return)

				return *perform_pointer_reinterpret_cast<
				    add_pointer_t<TargetType>>(sourcePtr);
			}
		}

		/*! Get the type member of the struct pointed/referred to by o.
		 *
		 * That is, performs o->type or o.type, as appropriate.
		 *
		 * For use in generic code that might get a pointer or a
		 * reference type and doesn't want to duplicate implementation.
		 */
		template <typename T>
		constexpr XrStructureType
		get_type(T&& o, std::enable_if_t<traits::is_xr_tagged_type_v<T>,
		                                 void*> /*only for sfinae*/
		                = nullptr)
		{
			static_assert(
			    traits::is_xr_tagged_type_v<T>,
			    "Can only use get_type() on a pointer/reference to "
			    "a (possibly const) XR tagged type.");
			if constexpr (is_pointer_v<
			                  std::remove_reference_t<T>>) {
				return o->type;
			} else { // NOLINT(readability-else-after-return)
				return o.type;
			}
		}
	} // namespace detail

#endif // !XRTRAITS_DOXYGEN

} // namespace casts

} // namespace xrtraits
#endif // XRTRAITS_HAVE_CONSTEXPR_IF

