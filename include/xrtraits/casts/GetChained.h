// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing xr_get_chained_struct, to extract a struct of a
 * given type from a next chain
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "../Common.h"

#ifdef XRTRAITS_HAVE_CONSTEXPR_IF

// Internal Includes
#include "TaggedCastCommon.h"
#include "TaggedRiskyCast.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace xrtraits {

namespace casts {
#ifndef XRTRAITS_DOXYGEN
	namespace detail {
		using ::std::conditional_t;
		using ::std::enable_if_t;
		using ::std::is_const;
		using ::std::is_pointer;
		using ::std::is_reference;
		using ::std::is_same;
		using ::std::remove_pointer_t;

		/*! Either const void* or void* depending on whether the
		 * source type is a pointer to const or not.
		 */
		template <typename SourceType>
		using possibly_const_void_ptr =
		    conditional_t<is_const<remove_pointer_t<SourceType>>::value,
		                  const void*, void*>;

		/*! Given a non-null const void * or void * pointing to
		 * an OpenXR tagged type, retrieve its nested next
		 * pointer, casted to XrBaseInStructure const* or
		 * XrBaseOutStructure* depending on the input constness.
		 */
		template <typename SourceType>
		inline auto risky_get_next(SourceType source)
		{
			static_assert(
			    is_same_v<SourceType, const void*> ||
			        is_same_v<SourceType, void*>,
			    "risky_get_next is only for void* or const void*");
			return cast_to_base(source)->next;
		}

		/*! Helper function template that performs all static
		 * assertions for xr_get_chained_struct
		 */
		template <typename TargetType, typename SourceType>
		inline void xr_get_chained_struct_static_assertions()
		{
			constexpr bool is_source_void =
			    is_same_v<remove_reference_pointer_cv_t<SourceType>,
			              void>;
			constexpr bool is_source_xr_tagged =
			    traits::is_xr_tagged_type_v<SourceType>;
			constexpr bool is_source_void_or_xr_tagged =
			    is_source_void || is_source_xr_tagged;

			// This operation requires either a `[const]
			// void *` input (which it assumes points to an
			// XR tagged type), or a static type that is
			// verifiably an XR tagged type.
			static_assert(
			    is_pointer_v<SourceType> &&
			        is_source_void_or_xr_tagged,
			    "Can only use xr_get_chained_struct to traverse "
			    "starting from a pointer to (possibly const) void, "
			    "or from a pointer to a (possibly const) XR tagged "
			    "type.");
			static_assert(
			    is_pointer_v<TargetType> &&
			        traits::is_xr_tagged_type_v<TargetType>,
			    "Can only use xr_get_chained_struct to "
			    "find and cast to a pointer to a "
			    "(possibly const) XR tagged type.");
			// Function determines whether to perform
			// reinterpret or return nullptr based on
			// comparing value of `type` to expected value
			// for the target type, so there must actually
			// be such an expected value. (Note that we
			// don't require this for the source type.)
			static_assert(
			    traits::has_xr_type_tag_v<TargetType>,
			    "Can only use xr_get_chained_struct to find and "
			    "cast to a pointer/reference to a concrete XR "
			    "tagged type with a known, defined type tag value "
			    "(XrStructureType enum value).");
			static_assert(
			    std::is_const<
			        remove_reference_pointer_t<SourceType>>::value
			        ? std::is_const<remove_reference_pointer_t<
			              TargetType>>::value
			        : true,
			    "Cannot remove const qualification through "
			    "xr_get_chained_struct");
		}

		/*! Core implementation of xr_get_chained_struct() that
		 * takes only void * or const void * as input, to unify
		 * cases and reduce template instantiations.
		 */
		template <typename TargetType, typename SourceType>
		inline TargetType xr_get_chained_struct_impl(SourceType src)
		{
			while (nullptr != src) {
				auto ret =
				    xr_tagged_risky_cast<TargetType>(src);
				if (ret) {
					// we found a match!
					return ret;
				}
				// no type match, so advance.
				src = risky_get_next(src);
			}
			// If we get here, the chain has ended: we're at
			// a nullptr
			return nullptr;
		}
	} // namespace detail
#endif    // !XRTRAITS_DOXYGEN
	/*! Follow the chained next pointers in an OpenXR tagged struct
	 * until we reach the end or find one matching the desired type.
	 *
	 * If no desired type is given, it is assumed to be the same as
	 * the source type, as is used in API entry point
	 * implementations.
	 *
	 * Given a type that is a pointer to a (possibly const) OpenXR
	 * tagged struct with known tag, and an input pointer that is
	 * either (possibly const) void * or a pointer to a (possibly
	 * const) OpenXR tagged struct, iterate through the current
	 * struct and all chained structs found via the next pointer,
	 * until a nullptr is hit or we find one whose tag matches the
	 * tag expected for the target type.
	 *
	 * If a match is found, it is casted to the same "const-ness" as
	 * the input and returned. If no match is found, then nullptr is
	 * returned.
	 *
	 * Unlike xr_tagged_risky_cast() and xr_tagged_dynamic_cast(),
	 * this function has a high likelihood of not finding what it is
	 * looking for, so there is no support for returning a reference
	 * type.
	 *
	 * @see xr_tagged_risky_cast() to which this function is closely
	 * related
	 *
	 * Source parameter is specified as `SourceType * src` instead
	 * of `SourceType src` to avoid getting std::nullptr_t as
	 * SourceType.
	 *
	 * @ingroup Casts
	 */
	template <typename TargetType, typename SourceType>
	inline TargetType xr_get_chained_struct(SourceType* src)
	{
		// Verify static assertions.
		detail::xr_get_chained_struct_static_assertions<TargetType,
		                                                SourceType*>();
		// turn SourceType into void * or const void *,
		// whichever is more accurate
		using type_erased_input_ptr =
		    detail::possibly_const_void_ptr<SourceType*>;
		// call the implementation function with the [const]
		// void * casted input
		return detail::xr_get_chained_struct_impl<TargetType>(
		    static_cast<type_erased_input_ptr>(src));
	}

	/*! Overload that doesn't require explicit specification of a
	 * template parameter, for "converting" a type to the type it
	 * already is.
	 *
	 * @ingroup Casts
	 */
	template <typename T> inline T* xr_get_chained_struct(T* src)
	{
		// Verify static assertions.
		detail::xr_get_chained_struct_static_assertions<T*, T*>();

		// turn T* into void * or const void *, whichever is
		// more accurate
		using type_erased_input_ptr =
		    detail::possibly_const_void_ptr<T*>;
		// call the implementation function with the [const]
		// void * casted input
		return detail::xr_get_chained_struct_impl<T*>(
		    static_cast<type_erased_input_ptr>(src));
	}
} // namespace casts

} // namespace xrtraits

#endif // XRTRAITS_HAVE_CONSTEXPR_IF
