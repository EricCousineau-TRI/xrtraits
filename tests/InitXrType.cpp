// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Testing of InitXrType.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/InitXrType.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

using namespace xrtraits;

TEST_CASE("initXrType_storage")
{
	WHEN("Instantiating a type")
	{
		auto storage = malloc(sizeof(XrSystemProperties));
		THEN("Should operate without throwing.") { REQUIRE_NOTHROW(initXrType<XrSystemProperties>(storage)); }
		auto typed = initXrType<XrSystemProperties>(storage);
		THEN("Contents should be correctly initialized")
		{
			CHECK(typed->systemId == 0);
			CHECK(typed->type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(typed->next == nullptr);
		}
	}
}

TEST_CASE("initXrType_ref")
{
	WHEN("Instantiating a type")
	{
		XrSystemProperties info;
		THEN("Should operate without throwing.") { REQUIRE_NOTHROW(initXrType(info)); }
		auto typed = initXrType(info);
		THEN("Contents should be correctly initialized")
		{
			CHECK(typed->systemId == 0);
			CHECK(typed->type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(typed->next == nullptr);

			CHECK(info.systemId == 0);
			CHECK(info.type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(info.next == nullptr);
		}
	}
}

TEST_CASE("initXrType_pointer")
{
	WHEN("Instantiating a type")
	{
		XrSystemProperties info;
		THEN("Should operate without throwing.") { REQUIRE_NOTHROW(initXrType(&info)); }
		auto typed = initXrType(&info);
		THEN("Contents should be correctly initialized")
		{
			CHECK(typed->systemId == 0);
			CHECK(typed->type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(typed->next == nullptr);

			CHECK(info.systemId == 0);
			CHECK(info.type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(info.next == nullptr);
		}
	}
}

TEST_CASE("Initialized")
{
	WHEN("Instantiating a type")
	{
		THEN("Should operate without throwing.") { REQUIRE_NOTHROW(Initialized<XrSystemProperties>{}); }
		Initialized<XrSystemProperties> info;
		THEN("Contents should be correctly initialized")
		{
			CHECK(info.systemId == 0);

			// accessing through get() to dodge the blocking
			// of type and next provided by Initialized.
			CHECK(info.get()->type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(info.get()->next == nullptr);
		}
	}
	static const XrSystemId systemId = 1;
	WHEN("Instantiating a type with one parameter")
	{
		THEN("Should operate without throwing.") { REQUIRE_NOTHROW(Initialized<XrSystemProperties>{systemId}); }
		Initialized<XrSystemProperties> info{systemId};
		THEN("Contents should be correctly initialized")
		{
			CHECK(info.systemId == systemId);

			// accessing through get() to dodge the blocking
			// of type and next provided by Initialized.
			CHECK(info.get()->type == traits::xr_type_tag_v<XrSystemProperties>);
			CHECK(info.get()->next == nullptr);
		}
	}

	WHEN("Instantiating a type with a chained structure")
	{
		AND_WHEN("Chained structure is an Initialized<T>")
		{
			Initialized<XrInstanceCreateInfo> chained;
			THEN("Should operate without throwing.")
			{
				REQUIRE_NOTHROW(Initialized<XrSystemProperties>{chained});
			}
			auto info = Initialized<XrSystemProperties>{chained};
			THEN("Contents should be correctly initialized")
			{
				CHECK(info.systemId == 0);

				// accessing through get() to dodge the
				// blocking of type and next provided by
				// Initialized.
				CHECK(info.get()->type == traits::xr_type_tag_v<XrSystemProperties>);
				CHECK(info.get()->next == &chained);
			}
			THEN("Contents of chained structed should be correct "
			     "and unmodified")
			{
				// accessing through get() to dodge the
				// blocking of type and next provided by
				// Initialized.
				CHECK(chained.get()->type == traits::xr_type_tag_v<XrInstanceCreateInfo>);
				CHECK(chained.get()->next == nullptr);
			}
		}

#ifdef XRTRAITS_HAVE_DYNAMIC_VERIFIED
		AND_WHEN("Chained structure is passed as DynamicVerified")
		{
			Initialized<XrInstanceCreateInfo> chained;
			auto chainedDynVerif = chained.asVerified();
			THEN("Should operate without throwing.")
			{
				REQUIRE_NOTHROW(Initialized<XrSystemProperties>{chainedDynVerif});
			}
			auto info = Initialized<XrSystemProperties>{chainedDynVerif};
			THEN("Contents should be correctly initialized")
			{
				CHECK(info.systemId == 0);

				// accessing through get() to dodge the
				// blocking of type and next provided by
				// Initialized.
				CHECK(info.get()->type == traits::xr_type_tag_v<XrSystemProperties>);
				CHECK(info.get()->next == &chained);
			}
			THEN("Contents of chained structed should be correct "
			     "and unmodified")
			{
				CHECK(chainedDynVerif->type == traits::xr_type_tag_v<XrInstanceCreateInfo>);
				CHECK(chainedDynVerif->next == nullptr);
			}
		}
#endif // XRTRAITS_HAVE_DYNAMIC_VERIFIED
	}

	WHEN("Instantiating a type with a chained structure and an initializer")
	{
		AND_WHEN("Chained structure is an Initialized<T>")
		{
			Initialized<XrInstanceCreateInfo> chained;
			THEN("Should operate without throwing.")
			{
				REQUIRE_NOTHROW(Initialized<XrSystemProperties>{chained, systemId});
			}
			auto info = Initialized<XrSystemProperties>{chained, systemId};
			THEN("Contents should be correctly initialized")
			{
				CHECK(info.systemId == systemId);

				// accessing through get() to dodge the
				// blocking of type and next provided by
				// Initialized.
				CHECK(info.get()->type == traits::xr_type_tag_v<XrSystemProperties>);
				CHECK(info.get()->next == &chained);
			}
			THEN("Contents of chained structed should be correct "
			     "and unmodified")
			{
				// accessing through get() to dodge the
				// blocking of type and next provided by
				// Initialized.
				CHECK(chained.get()->type == traits::xr_type_tag_v<XrInstanceCreateInfo>);
				CHECK(chained.get()->next == nullptr);
			}
		}

#ifdef XRTRAITS_HAVE_DYNAMIC_VERIFIED
		AND_WHEN("Chained structure is passed as DynamicVerified")
		{
			Initialized<XrInstanceCreateInfo> chained;
			auto chainedDynVerif = chained.asVerified();
			THEN("Should operate without throwing.")
			{
				REQUIRE_NOTHROW(Initialized<XrSystemProperties>{chainedDynVerif});
			}
			auto info = Initialized<XrSystemProperties>{chainedDynVerif};
			THEN("Contents should be correctly initialized")
			{
				CHECK(info.systemId == 0);

				// accessing through get() to dodge the
				// blocking of type and next provided by
				// Initialized.
				CHECK(info.get()->type == traits::xr_type_tag_v<XrSystemProperties>);
				CHECK(info.get()->next == &chained);
			}
			THEN("Contents of chained structed should be correct "
			     "and unmodified")
			{
				CHECK(chainedDynVerif->type == traits::xr_type_tag_v<XrInstanceCreateInfo>);
				CHECK(chainedDynVerif->next == nullptr);
			}
		}
#endif // XRTRAITS_HAVE_DYNAMIC_VERIFIED
	}
}
