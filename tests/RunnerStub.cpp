// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Dummy file to contain catch main test runner implementation.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

