// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for xrtraits/APITraits.h
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <openxr/openxr.h>

// Internal Includes
#include "xrtraits/traits/APITraits.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
#include <string>

using namespace xrtraits;
using namespace std::string_literals;

TEST_CASE("XrStructureType_to_string")
{
	WHEN("Passing XR_TYPE_INSTANCE_CREATE_INFO")
	{
		THEN("Should return \"XR_TYPE_INSTANCE_CREATE_INFO\"")
		{
			CHECK("XR_TYPE_INSTANCE_CREATE_INFO"s == traits::to_string(XR_TYPE_INSTANCE_CREATE_INFO));
		}
	}
	WHEN("Passing an not-valid enum (XR_STRUCTURE_TYPE_MAX_ENUM)")
	{
		THEN("Should return nullptr") { CHECK(nullptr == traits::to_string(XR_STRUCTURE_TYPE_MAX_ENUM)); }
	}
}

TEST_CASE("XrStructureType_getTypeName")
{
	WHEN("Passing XR_TYPE_INSTANCE_CREATE_INFO")
	{
		THEN("Should return \"XrInstanceCreateInfo\"")
		{
			CHECK("XrInstanceCreateInfo"s == traits::getTypeName(XR_TYPE_INSTANCE_CREATE_INFO));
		}
	}
	WHEN("Passing an not-valid enum (XR_STRUCTURE_TYPE_MAX_ENUM)")
	{
		THEN("Should return nullptr") { CHECK(nullptr == traits::getTypeName(XR_STRUCTURE_TYPE_MAX_ENUM)); }
	}
}

TEST_CASE("is_xr_tagged_type_v")
{
	CHECK(traits::is_xr_tagged_type_v<XrInstanceCreateInfo>);
	CHECK(traits::is_xr_tagged_type_v<XrBaseInStructure>);
	CHECK_FALSE(traits::is_xr_tagged_type_v<XrVector3f>);
}

TEST_CASE("has_xr_type_tag_v")
{
	CHECK(traits::has_xr_type_tag_v<XrInstanceCreateInfo>);
	CHECK_FALSE(traits::has_xr_type_tag_v<XrBaseInStructure>);
	CHECK_FALSE(traits::has_xr_type_tag_v<XrVector3f>);
}
