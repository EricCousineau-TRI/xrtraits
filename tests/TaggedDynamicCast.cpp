// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 *  @brief  Test of TaggedDynamicCast.h
 *  @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/casts/TaggedDynamicCast.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

using namespace xrtraits;
using xrtraits::casts::xr_tagged_dynamic_cast;

TEST_CASE("xr_tagged_dynamic_cast")
{
	WHEN("accessing a concrete tagged input type (XrInstanceCreateInfo)")
	{
		XrInstanceCreateInfo realObj = {XR_TYPE_INSTANCE_CREATE_INFO};
		auto realPtr = &realObj;

		THEN("get valid result when casting to pointer to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(realPtr));
			REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(realPtr) != nullptr);
			REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(realPtr) == realPtr);
		}
		THEN("get valid result when casting to pointer to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(realPtr));
			REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(realPtr) != nullptr);
			REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(realPtr) == realPtr);
		}
		auto constPtr = const_cast<XrInstanceCreateInfo const*>(realPtr);
		THEN("get valid result when casting to pointer to correct type (as const) from const source")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(constPtr));
			REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(constPtr) != nullptr);
			REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(constPtr) == realPtr);
		}

		THEN("correct result and no exception when casting to reference to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo&>(realPtr));
			REQUIRE(&xr_tagged_dynamic_cast<XrInstanceCreateInfo&>(realPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to reference to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(realPtr));
			REQUIRE(&xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(realPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to pointer to correct type (as const) from const "
		     "source")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(constPtr));
			REQUIRE(&xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(constPtr) == realPtr);
		}
		AND_WHEN("using a pointer-to-different-abstract-base (that is still compatible - XrBaseInStructure) as "
		         "source")
		{

			auto diffCompatiblePtr = reinterpret_cast<XrBaseInStructure*>(realPtr);
			THEN("get valid result when casting to pointer to correct type")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(diffCompatiblePtr));
				REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(diffCompatiblePtr) != nullptr);
				REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(diffCompatiblePtr) == realPtr);
			}
			THEN("get valid result when casting to pointer to correct type (as const)")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(diffCompatiblePtr));
				REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(diffCompatiblePtr) !=
				        nullptr);
				REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(diffCompatiblePtr) ==
				        realPtr);
			}
			auto constDiffCompatiblePtr = const_cast<XrBaseInStructure const*>(diffCompatiblePtr);
			THEN("get valid result when casting to pointer to correct type (as const) from const source")
			{
				REQUIRE_NOTHROW(
				    xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(constDiffCompatiblePtr));
				REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(constDiffCompatiblePtr) !=
				        nullptr);
				REQUIRE(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(constDiffCompatiblePtr) ==
				        realPtr);
			}

			THEN("correct result and no exception when casting to reference to correct type")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo&>(diffCompatiblePtr));
				REQUIRE(&xr_tagged_dynamic_cast<XrInstanceCreateInfo&>(diffCompatiblePtr) == realPtr);
			}
			THEN("correct result and no exception when casting to reference to correct type (as const)")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(diffCompatiblePtr));
				REQUIRE(&xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(diffCompatiblePtr) ==
				        realPtr);
			}
			THEN("correct result and no exception when casting to pointer to correct type (as const) from "
			     "const source")
			{
				REQUIRE_NOTHROW(
				    xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(constDiffCompatiblePtr));
				REQUIRE(&xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(constDiffCompatiblePtr) ==
				        realPtr);
			}
		}
		AND_WHEN("casting to the wrong pointer type")
		{
			THEN("nullptr returned")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState*>(realPtr));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrFrameState*>(realPtr));
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const*>(realPtr));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrFrameState const*>(realPtr));
			}
		}
		AND_WHEN("casting to the wrong reference type")
		{
			THEN("throws")
			{
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrFrameState&>(realPtr));
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrFrameState const&>(realPtr));
			}
		}
	}

	WHEN("accessing a concrete tagged output type (XrFrameState)")
	{
		XrFrameState realObj = {XR_TYPE_FRAME_STATE};
		auto realPtr = &realObj;

		THEN("get valid result when casting to pointer to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState*>(realPtr));
			REQUIRE(xr_tagged_dynamic_cast<XrFrameState*>(realPtr) != nullptr);
			REQUIRE(xr_tagged_dynamic_cast<XrFrameState*>(realPtr) == realPtr);
		}
		THEN("get valid result when casting to pointer to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const*>(realPtr));
			REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(realPtr) != nullptr);
			REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(realPtr) == realPtr);
		}
		auto constPtr = const_cast<XrFrameState const*>(realPtr);
		THEN("get valid result when casting to pointer to correct type (as const) from const source")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const*>(constPtr));
			REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(constPtr) != nullptr);
			REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(constPtr) == realPtr);
		}

		THEN("correct result and no exception when casting to reference to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState&>(realPtr));
			REQUIRE(&xr_tagged_dynamic_cast<XrFrameState&>(realPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to reference to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const&>(realPtr));
			REQUIRE(&xr_tagged_dynamic_cast<XrFrameState const&>(realPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to pointer to correct type (as const) from const "
		     "source")
		{
			REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const&>(constPtr));
			REQUIRE(&xr_tagged_dynamic_cast<XrFrameState const&>(constPtr) == realPtr);
		}
		AND_WHEN(
		    "using a pointer-to-different-abstract-base (that is still compatible - XrBaseOutStructure) as "
		    "source")
		{

			auto diffCompatiblePtr = reinterpret_cast<XrBaseOutStructure*>(realPtr);
			THEN("get valid result when casting to pointer to correct type")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState*>(diffCompatiblePtr));
				REQUIRE(xr_tagged_dynamic_cast<XrFrameState*>(diffCompatiblePtr) != nullptr);
				REQUIRE(xr_tagged_dynamic_cast<XrFrameState*>(diffCompatiblePtr) == realPtr);
			}
			THEN("get valid result when casting to pointer to correct type (as const)")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const*>(diffCompatiblePtr));
				REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(diffCompatiblePtr) != nullptr);
				REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(diffCompatiblePtr) == realPtr);
			}
			auto constDiffCompatiblePtr = const_cast<XrBaseOutStructure const*>(diffCompatiblePtr);
			THEN("get valid result when casting to pointer to correct type (as const) from const source")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const*>(constDiffCompatiblePtr));
				REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(constDiffCompatiblePtr) != nullptr);
				REQUIRE(xr_tagged_dynamic_cast<XrFrameState const*>(constDiffCompatiblePtr) == realPtr);
			}

			THEN("correct result and no exception when casting to reference to correct type")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState&>(diffCompatiblePtr));
				REQUIRE(&xr_tagged_dynamic_cast<XrFrameState&>(diffCompatiblePtr) == realPtr);
			}
			THEN("correct result and no exception when casting to reference to correct type (as const)")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const&>(diffCompatiblePtr));
				REQUIRE(&xr_tagged_dynamic_cast<XrFrameState const&>(diffCompatiblePtr) == realPtr);
			}
			THEN("correct result and no exception when casting to pointer to correct type (as const) from "
			     "const source")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const&>(constDiffCompatiblePtr));
				REQUIRE(&xr_tagged_dynamic_cast<XrFrameState const&>(constDiffCompatiblePtr) ==
				        realPtr);
			}
		}
		AND_WHEN("casting to the wrong pointer type")
		{
			THEN("nullptr returned")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(realPtr));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(realPtr));
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(realPtr));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(realPtr));
			}
		}
		AND_WHEN("casting to the wrong reference type")
		{
			THEN("throws")
			{
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrInstanceCreateInfo&>(realPtr));
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(realPtr));
			}
		}
	}
	WHEN("passing in nullptr")
	{
		XrInstanceCreateInfo* nullPointer = nullptr;
		AND_WHEN("casting to a pointer type")
		{
			THEN("nullptr returned")
			{
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(nullPointer));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrInstanceCreateInfo*>(nullPointer));
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState*>(nullPointer));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrFrameState*>(nullPointer));
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(nullPointer));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrInstanceCreateInfo const*>(nullPointer));
				REQUIRE_NOTHROW(xr_tagged_dynamic_cast<XrFrameState const*>(nullPointer));
				REQUIRE(nullptr == xr_tagged_dynamic_cast<XrFrameState const*>(nullPointer));
			}
		}

		AND_WHEN("casting to a reference type")
		{
			THEN("throws")
			{
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrInstanceCreateInfo&>(nullPointer));
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrFrameState&>(nullPointer));
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrInstanceCreateInfo const&>(nullPointer));
				REQUIRE_THROWS(xr_tagged_dynamic_cast<XrFrameState const&>(nullPointer));
			}
		}
	}
}
