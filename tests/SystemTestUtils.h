// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#if 0
// Internal Includes
#include "xrtraits/InitXrType.h"
#include "OpenXR_Wrapper.h"
#include "xr_generated_utilities.h"

// Library Includes
#include <catch2/catch.hpp>
#include <util/FixedLengthStringFunctions.h>

// Standard Includes
#include <algorithm>
#include <functional>
#include <initializer_list>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include <vector>

using namespace xrtraits;

struct XrResultCode
{
	XrResult result;
};

inline bool operator==(XrResultCode const& lhs, XrResultCode const& rhs) { return lhs.result == rhs.result; }

inline std::ostream& operator<<(std::ostream& os, XrResultCode const& code)
{
	char buffer[XR_MAX_RESULT_STRING_SIZE] = {};
	GeneratedXrUtilitiesResultToString(code.result, buffer);
	os << buffer;
	return os;
}

/// Perform the two call idiom - pass a lambda that takes the capacityInput, countOutput, and array parameters.
template <typename T, typename F> inline std::vector<T> checkTwoCall(F&& wrappedCall)
{
	uint32_t count = 0;
	std::vector<T> ret;
	REQUIRE(XrResultCode{XR_SUCCESS} == XrResultCode{wrappedCall(0, &count, nullptr)});
	if (count > 0) {
		ret.resize(count);
		if constexpr (traits::is_xr_tagged_type_v<T>) {
			// we need to initialize the OpenXR tagged type.
			for (auto& elt : ret) {
				initXrType(elt);
			}
		}
		REQUIRE(XrResultCode{XR_SUCCESS} == XrResultCode{wrappedCall(ret.size(), &count, ret.data())});
		REQUIRE(count > 0);
		ret.resize(count);
	}
	return ret;
}
#ifdef XRTRAITS_USE_OPENXR_LOADER
// Wrappers for when we are using the loader and thus are mangling entry point names by prefixing with xrtraits_

#define CHECK_RESULT(EXPECTED, CALL, PARAMS) CHECK(XrResultCode{EXPECTED} == XrResultCode{xrtraits_##CALL PARAMS})
#define REQUIRE_RESULT(EXPECTED, CALL, PARAMS) REQUIRE(XrResultCode{EXPECTED} == XrResultCode{xrtraits_##CALL PARAMS})
#define CHECK_RESULT_FALSE(EXPECTED, CALL, PARAMS)                                                                     \
	CHECK_FALSE(XrResultCode{EXPECTED} == XrResultCode{xrtraits_##CALL PARAMS})
#define REQUIRE_RESULT_FALSE(EXPECTED, CALL, PARAMS)                                                                   \
	REQUIRE_FALSE(XrResultCode{EXPECTED} == XrResultCode{xrtraits_##CALL PARAMS})

#else // XRTRAITS_USE_OPENXR_LOADER

// Wrappers for when we aren't using the loader and thus aren't mangling entry point names.

#define CHECK_RESULT(EXPECTED, CALL, PARAMS) CHECK(XrResultCode{EXPECTED} == XrResultCode{CALL PARAMS})
#define REQUIRE_RESULT(EXPECTED, CALL, PARAMS) REQUIRE(XrResultCode{EXPECTED} == XrResultCode{CALL PARAMS})
#define CHECK_RESULT_FALSE(EXPECTED, CALL, PARAMS) CHECK_FALSE(XrResultCode{EXPECTED} == XrResultCode{CALL PARAMS})
#define REQUIRE_RESULT_FALSE(EXPECTED, CALL, PARAMS) REQUIRE_FALSE(XrResultCode{EXPECTED} == XrResultCode{CALL PARAMS})

#endif // XRTRAITS_USE_OPENXR_LOADER

struct PopulatedInstanceCreateInfo
{
	PopulatedInstanceCreateInfo(const char* ext) : extension(ext)
	{
		util::strcpy_safe(instance_create_info.applicationInfo.applicationName, "SystemTest");
		instance_create_info.applicationInfo.applicationVersion = XR_MAKE_VERSION(1, 0, 0);
		util::strcpy_safe(instance_create_info.applicationInfo.engineName, "NoEngine");
		instance_create_info.applicationInfo.engineVersion = XR_MAKE_VERSION(1, 0, 0);
		instance_create_info.applicationInfo.apiVersion = XR_CURRENT_API_VERSION;
		instance_create_info.enabledExtensionNames = extension_name_array;
		instance_create_info.enabledExtensionCount = 1;
	}
	const char* extension;
	const char* extension_name_array[1] = {extension};

	Initialized<XrInstanceCreateInfo> instance_create_info;
};

/// Custom matcher for use with ???_THAT() assertions, which takes a user-provided predicate and checks for at least one
/// element in the collection for which this is true.
template <typename T> class ContainsPredicate : public Catch::MatcherBase<T>
{
public:
	using ValueType = typename T::value_type;
	using PredicateType = std::function<bool(ValueType const&)>;
	ContainsPredicate(PredicateType&& predicate, const char* desc) : predicate_(std::move(predicate)), desc_(desc)
	{}

	virtual bool match(T const& container) const override
	{
		using std::begin;
		using std::end;
		return end(container) != std::find_if(begin(container), end(container), predicate_);
	}

	virtual std::string describe() const override
	{
		using namespace std::string_literals;
		return "contains an element such that "s + desc_;
	}

private:
	PredicateType predicate_;
	const char* desc_;
};

template <typename T> using VectorContainsPredicate = ContainsPredicate<std::vector<T>>;

/// Custom matcher for use with ???_THAT() assertions, which takes an initializer_list of permitted values and ensures
/// the checked value is one of those.
template <typename T> class In : public Catch::MatcherBase<T>
{
public:
	In(std::initializer_list<T> permittedValues) : permittedValues_(permittedValues) {}

	virtual bool match(T const& val) const override
	{
		using std::begin;
		using std::end;
		return end(permittedValues_) != std::find(begin(permittedValues_), end(permittedValues_), val);
	}

	virtual std::string describe() const override
	{
		std::ostringstream os;
		os << "is one of {";
		bool needComma = false;
		for (auto& val : permittedValues_) {
			os << val;
			if (needComma) {
				os << ", ";
			}
			needComma = true;
		}
		os << "}";
		return os.str();
	}

private:
	std::initializer_list<T> permittedValues_;
};

#endif
