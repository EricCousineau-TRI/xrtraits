// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*! @file
 *  @brief  Test of TaggedDynamicCast.h
 *  @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/casts/TaggedRiskyCast.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

using namespace xrtraits;
using xrtraits::casts::xr_tagged_risky_cast;

TEST_CASE("xr_tagged_risky_cast")
{
	WHEN("accessing a concrete tagged input type (XrInstanceCreateInfo)")
	{
		XrInstanceCreateInfo realObj = {XR_TYPE_INSTANCE_CREATE_INFO};
		auto realPtr = &realObj;

		auto voidPtr = reinterpret_cast<void*>(realPtr);
		THEN("get valid result when casting to pointer to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo*>(voidPtr));
			REQUIRE(xr_tagged_risky_cast<XrInstanceCreateInfo*>(voidPtr) != nullptr);
			REQUIRE(xr_tagged_risky_cast<XrInstanceCreateInfo*>(voidPtr) == realPtr);
		}
		THEN("get valid result when casting to pointer to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo const*>(voidPtr));
			REQUIRE(xr_tagged_risky_cast<XrInstanceCreateInfo const*>(voidPtr) != nullptr);
			REQUIRE(xr_tagged_risky_cast<XrInstanceCreateInfo const*>(voidPtr) == realPtr);
		}
		auto constVoidPtr = const_cast<void const*>(voidPtr);
		THEN("get valid result when casting to pointer to correct type (as const) from const void *")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo const*>(constVoidPtr));
			REQUIRE(xr_tagged_risky_cast<XrInstanceCreateInfo const*>(constVoidPtr) != nullptr);
			REQUIRE(xr_tagged_risky_cast<XrInstanceCreateInfo const*>(constVoidPtr) == realPtr);
		}

		THEN("correct result and no exception when casting to reference to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo&>(voidPtr));
			REQUIRE(&xr_tagged_risky_cast<XrInstanceCreateInfo&>(voidPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to reference to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo const&>(voidPtr));
			REQUIRE(&xr_tagged_risky_cast<XrInstanceCreateInfo const&>(voidPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to pointer to correct type (as const) from const "
		     "source")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo const&>(constVoidPtr));
			REQUIRE(&xr_tagged_risky_cast<XrInstanceCreateInfo const&>(constVoidPtr) == realPtr);
		}

		AND_WHEN("casting to the wrong pointer type")
		{
			THEN("nullptr returned")
			{
				REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState*>(voidPtr));
				REQUIRE(nullptr == xr_tagged_risky_cast<XrFrameState*>(voidPtr));
			}
		}
		AND_WHEN("casting to the wrong reference type")
		{
			THEN("throws") { REQUIRE_THROWS(xr_tagged_risky_cast<XrFrameState&>(voidPtr)); }
		}
	}

	WHEN("accessing a concrete tagged output type (XrFrameState)")
	{
		XrFrameState realObj = {XR_TYPE_FRAME_STATE};
		auto realPtr = &realObj;
		auto voidPtr = reinterpret_cast<void*>(realPtr);
		THEN("get valid result when casting to pointer to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState*>(voidPtr));
			REQUIRE(xr_tagged_risky_cast<XrFrameState*>(voidPtr) != nullptr);
			REQUIRE(xr_tagged_risky_cast<XrFrameState*>(voidPtr) == realPtr);
		}
		THEN("get valid result when casting to pointer to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState const*>(voidPtr));
			REQUIRE(xr_tagged_risky_cast<XrFrameState const*>(voidPtr) != nullptr);
			REQUIRE(xr_tagged_risky_cast<XrFrameState const*>(voidPtr) == realPtr);
		}
		auto constVoidPtr = const_cast<void const*>(voidPtr);
		THEN("get valid result when casting to pointer to correct type (as const) from const void *")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState const*>(constVoidPtr));
			REQUIRE(xr_tagged_risky_cast<XrFrameState const*>(constVoidPtr) != nullptr);
			REQUIRE(xr_tagged_risky_cast<XrFrameState const*>(constVoidPtr) == realPtr);
		}

		THEN("correct result and no exception when casting to reference to correct type")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState&>(voidPtr));
			REQUIRE(&xr_tagged_risky_cast<XrFrameState&>(voidPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to reference to correct type (as const)")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState const&>(voidPtr));
			REQUIRE(&xr_tagged_risky_cast<XrFrameState const&>(voidPtr) == realPtr);
		}
		THEN("correct result and no exception when casting to pointer to correct type (as const) from const "
		     "source")
		{
			REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState const&>(constVoidPtr));
			REQUIRE(&xr_tagged_risky_cast<XrFrameState const&>(constVoidPtr) == realPtr);
		}
		AND_WHEN("casting to the wrong pointer type")
		{
			THEN("nullptr returned")
			{
				REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo*>(voidPtr));
				REQUIRE(nullptr == xr_tagged_risky_cast<XrInstanceCreateInfo*>(voidPtr));
			}
		}
		AND_WHEN("casting to the wrong reference type")
		{
			THEN("throws") { REQUIRE_THROWS(xr_tagged_risky_cast<XrInstanceCreateInfo&>(voidPtr)); }
		}
	}
	WHEN("passing in nullptr")
	{
		void* nullPointer = nullptr;
		AND_WHEN("casting to a pointer type")
		{
			THEN("nullptr returned")
			{
				REQUIRE_NOTHROW(xr_tagged_risky_cast<XrInstanceCreateInfo*>(nullPointer));
				REQUIRE(nullptr == xr_tagged_risky_cast<XrInstanceCreateInfo*>(nullPointer));
				REQUIRE_NOTHROW(xr_tagged_risky_cast<XrFrameState*>(nullPointer));
				REQUIRE(nullptr == xr_tagged_risky_cast<XrFrameState*>(nullPointer));
			}
		}

		AND_WHEN("casting to a reference type")
		{
			THEN("throws")
			{
				REQUIRE_THROWS(xr_tagged_risky_cast<XrInstanceCreateInfo&>(nullPointer));
				REQUIRE_THROWS(xr_tagged_risky_cast<XrFrameState&>(nullPointer));
			}
		}
	}
}
