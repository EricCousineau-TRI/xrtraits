// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/exceptions/BaseExceptions.h"

// Library Includes
#include "../FmtWrap.h"

// Standard Includes
#include <utility>

namespace xrtraits {
namespace exceptions {

	const char* xr_exception::what() const noexcept
	{
		return what_.c_str();
	}

	bool xr_exception::hasResult() const noexcept { return hasResult_; }

	XrResult xr_exception::result() const noexcept { return result_; }

	xr_exception::xr_exception(std::string&& what) : what_(std::move(what))
	{}

	xr_exception::xr_exception(std::string&& what, XrResult err)
	    : what_(std::move(what)), result_(err), hasResult_(true)
	{}

	xr_exception::xr_exception(std::string const& what, XrResult err)
	    : what_(what), result_(err), hasResult_(true)
	{}

	xr_runtime_error::xr_runtime_error(std::string&& what, XrResult err)
	    : xr_exception(std::move(what), err)
	{}

	xr_runtime_error::xr_runtime_error(std::string const& what,
	                                   XrResult err)
	    : xr_exception(what, err)
	{}

	xr_runtime_error::xr_runtime_error(std::string&& what)
	    : xr_exception(std::move(what))
	{}

	xr_runtime_error::xr_runtime_error(XrResult err, const char* condition,
	                                   const char* msg)
	    : xr_runtime_error(format("Got {}(XrResult({})) is false - {}",
	                              condition, static_cast<int>(err), msg),
	                       err)
	{}

	void throwIfNotSucceeded(XrResult result, const char* errorMessage)
	{
		if (!XR_SUCCEEDED(result)) {
			throw xr_runtime_error(result, "XR_SUCCEEDED",
			                       errorMessage);
		}
	}

	void throwIfNotUnqualifiedSuccess(XrResult result,
	                                  const char* errorMessage)
	{
		if (!XR_UNQUALIFIED_SUCCESS(result)) {
			throw xr_runtime_error(result, "XR_UNQUALIFIED_SUCCESS",
			                       errorMessage);
		}
	}

	xr_logic_error::xr_logic_error(std::string&& what, XrResult err)
	    : xr_exception(std::move(what), err)
	{}

	xr_logic_error::xr_logic_error(std::string const& what, XrResult err)
	    : xr_exception(what, err)
	{}

	xr_logic_error::xr_logic_error(std::string&& what)
	    : xr_exception(std::move(what))
	{}

} // namespace exceptions
} // namespace xrtraits
