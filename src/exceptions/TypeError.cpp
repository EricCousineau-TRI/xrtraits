// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/exceptions/TypeError.h"
#include "xrtraits/traits/APITraits.h"

// Library Includes
#include "../FmtWrap.h"

// Standard Includes
// - none

namespace xrtraits {

//! Wraps XrSTructureType for logging
struct StructureType
{
	//! The data member
	XrStructureType type;
};

} // namespace xrtraits

#ifndef XRTRAITS_DOXYGEN
namespace fmt {

// Explicit template specialization to provide a way to format XrStructureType
// values in a log message.
template <> struct formatter<xrtraits::StructureType>
{
	template <typename ParseContext> constexpr auto parse(ParseContext& ctx)
	{
		return ctx.begin();
	}

	template <typename FormatContext>
	auto format(xrtraits::StructureType const t, FormatContext& ctx)
	{
		auto typeName = xrtraits::traits::getTypeName(t.type);
		auto enumName = xrtraits::traits::to_string(t.type);
		if (typeName != nullptr && enumName != nullptr) {
			return format_to(ctx.begin(), "type {} (.type = {})",
			                 typeName, enumName);
		}
		return format_to(ctx.begin(), "unknown type (.type = {:#010x})",
		                 static_cast<std::uint64_t>(t.type));
	}
};

} // namespace fmt

#endif // !XRTRAITS_DOXYGEN

namespace xrtraits {
namespace exceptions {

	static inline std::string paramNameWrapper(const char* paramName)
	{
		if (paramName == nullptr) {
			return "Parameter";
		}
		return format("Parameter {}", paramName);
	}

	type_error::type_error(std::string&& msg, XrResult result)
	    : xr_logic_error(std::move(msg), result)
	{}

	type_error::type_error(const char* paramName, std::nullptr_t,
	                       XrStructureType expectedType, XrResult result)
	    : type_error(format("{} is nullptr instead of the expected {}",
	                        paramNameWrapper(paramName),
	                        StructureType{expectedType}),
	                 result)
	{}

	type_error::type_error(const char* paramName,
	                       XrStructureType actualType,
	                       XrStructureType expectedType, XrResult result)
	    : type_error(
	          format("{} is a struct of {} instead of the expected {}",
	                 paramNameWrapper(paramName), StructureType{actualType},
	                 StructureType{expectedType}),
	          result)
	{}

	static inline std::string paramIndexWrapper(const char* paramName,
	                                            std::ptrdiff_t index)
	{
		if (paramName == nullptr) {
			return format("Array parameter element {}", index);
		}
		return format("Array parameter element {}[{}]", paramName,
		              index);
	}

	type_error::type_error(const char* paramName,
	                       XrStructureType actualType,
	                       XrStructureType expectedType,
	                       std::ptrdiff_t index, XrResult result)
	    : type_error(
	          format("{} is a struct of {} instead of the expected {}",
	                 paramIndexWrapper(paramName, index),
	                 StructureType{actualType},
	                 StructureType{expectedType}),
	          result)
	{}

	type_error::type_error(const char* paramName, std::nullptr_t,
	                       XrStructureType expectedType,
	                       std::ptrdiff_t size, XrResult result)
	    : type_error(format("{} is nullptr instead of an array of the "
	                        "expected {} (size given was {})",
	                        paramNameWrapper(paramName),
	                        StructureType{expectedType}, size),
	                 result)
	{}

	get_from_chain_error::get_from_chain_error(std::string&& msg,
	                                           XrResult result)
	    : type_error(std::move(msg), result)
	{}

	get_from_chain_error::get_from_chain_error(const char* paramName,
	                                           std::nullptr_t,
	                                           XrStructureType expectedType,
	                                           XrResult result)
	    : get_from_chain_error(format("{} is nullptr instead of a chain "
	                                  "containing the expected {}",
	                                  paramNameWrapper(paramName),
	                                  StructureType{expectedType}),
	                           result)
	{}

	get_from_chain_error::get_from_chain_error(const char* paramName,
	                                           XrStructureType headType,
	                                           XrStructureType expectedType,
	                                           XrResult result)
	    : get_from_chain_error(
	          format("{} is a chain starting with {} that did "
	                 "not include the expected {}",
	                 paramNameWrapper(paramName), StructureType{headType},
	                 StructureType{expectedType}),
	          result)
	{}

} // namespace exceptions
} // namespace xrtraits
