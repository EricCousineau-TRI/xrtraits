// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/exceptions/CastExceptions.h"
#include "xrtraits/traits/APITraits.h"

// Library Includes
#include "../FmtWrap.h"

// Standard Includes
// - none

namespace xrtraits {
namespace exceptions {

	using traits::detail::structureTypeEnumToString;

	static inline auto badTaggedCastMessage(XrStructureType destTag,
	                                        XrStructureType sourceTag,
	                                        const char* castType)
	{
		auto sourceTagName = structureTypeEnumToString(sourceTag);
		if (sourceTagName == nullptr) {
			return format(
			    "Attempted {} to {} (by reference) failed: source "
			    "object had unrecognized tag value {:#010x}",
			    castType, structureTypeEnumToString(destTag),
			    sourceTag);
		}
		return format(
		    "Attempted {} to {} (by reference) failed: source object "
		    "had tag {}",
		    castType, structureTypeEnumToString(destTag),
		    sourceTagName);
	}

	bad_tagged_cast::bad_tagged_cast(XrStructureType destTag,
	                                 XrStructureType sourceTag,
	                                 const char* castType)
	    : xr_runtime_error(
	          badTaggedCastMessage(destTag, sourceTag, castType))
	{}

	bad_tagged_cast::bad_tagged_cast(XrStructureType destTag,
	                                 std::nullptr_t, const char* castType)
	    : xr_runtime_error(
	          format("Attempted {} to {} (by reference) failed: source was "
	                 "nullptr",
	                 castType, structureTypeEnumToString(destTag)))
	{}

} // namespace exceptions
} // namespace xrtraits
