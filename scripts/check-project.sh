#!/bin/bash
# Copyright 2018-2019, Collabora, Ltd.
# Copyright 2016, Sensics, Inc.
# SPDX-License-Identifier: BSL-1.0

# Run from within a build directory or set BUILD_DIR
# If you don't pass a filename, it will check all source files.

if [ ! "$RUN_CLANG_TIDY" ]; then
        for exe in run-clang-tidy-8 run-clang-tidy-7 run-clang-tidy-6.0 run-clang-tidy; do
                if which $exe >/dev/null 2>&1; then
                        RUN_CLANG_TIDY=$exe
                        break
                fi
        done
fi
if [ ! "$RUN_CLANG_TIDY" ]; then
        echo "Can't find run-clang-tidy - please set RUN_CLANG_TIDY to a command or path" >&2
        exit 1
fi

SRC_DIR=$(cd $(dirname $0) && cd ../src/xrtraits && pwd)
BUILD_DIR=${BUILD_DIR:-$(pwd)}
(
        if [ $# -gt 0 ]; then
                echo "$@"
        else
                cd $(dirname $0)/..
                find "src" \( -name "*.c" -o -name "*.cpp" \)
        fi
)| \
        grep -v "\.boilerplate" | \
        xargs ${RUN_CLANG_TIDY} -style=file -p "$BUILD_DIR" -header-filter="^$SRC_DIR/.*"
