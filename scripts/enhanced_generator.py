#!/usr/bin/python3 -i
#
# Copyright (c) 2013-2019 The Khronos Group Inc.
# Copyright (c) 2017-2019 Valve Corporation
# Copyright (c) 2017-2019 LunarG, Inc.
# Copyright 2018-2019, Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Mark Young <marky@lunarg.com>
#               Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      This file provides a number of shared features used by
#               source generators.

# Based in part on OpenXR src/scripts/utility_source_generator.py

import os
import re
import sys
from collections import namedtuple

from automatic_source_generator import AutomaticSourceOutputGenerator

# EnhancedAutoSourceOutputGenerator - subclass of AutomaticSourceOutputGenerator.


class EnhancedAutoSourceOutputGenerator(AutomaticSourceOutputGenerator):
    def __init__(self,
                 errFile=sys.stderr,
                 warnFile=sys.stderr,
                 diagFile=sys.stdout):
        AutomaticSourceOutputGenerator.__init__(
            self, errFile, warnFile, diagFile)
        self.indentString = '    '

    def indent(self, width=1):
        return self.indentString * (width + 1)

    def formatReturnType(self, cmd):
        if cmd.return_type is not None:
            return cmd.return_type.text
        return 'void'

    def formatParam(self, param):
        return param.cdecl.strip()

    def isTaggedType(self, structure_type):
        return structure_type.members[0].type == 'XrStructureType'

    def getStructureTag(self, structure_type):
        return structure_type.members[0].values

    def hasDefinedStructureTag(self, structure_type):
        return self.getStructureTag(structure_type) is not None

    def iterateCoreThenExtensions(self, interable):
        for isCore in [True, False]:
            elts = filter(lambda elt: self.isCoreExtensionName(
                elt.ext_name) == isCore)
            for elt in elts:
                yield elt

    def resetCoreExtensionTracking(self):
        '''Must be called at the beginning of any function that is
        iterating thru Core then Extensions (like using iterateCoreThenExtensions) and using processCoreExtensionComment'''
        self.cur_extension_name = ''

    def processCoreExtension(self, elt):
        this_ext_name = elt.ext_name
        if this_ext_name != self.cur_extension_name:
            self.cur_extension_name = this_ext_name
            self.is_extension_change = True
        else:
            self.is_extension_change = False

    def getCoreExtensionComment(self, indent):
        if self.is_extension_change:
            if self.isCoreExtensionName(self.cur_extension_name):
                # trim off some all-caps text to just get the version
                comment_contents = 'Core %s' % self.cur_extension_name[11:]
            else:
                # use whole extension name
                comment_contents = '%s extension' % self.cur_extension_name
            return '\n{leadingWhitespace}// ---- {sectionTitle}\n'.format(leadingWhitespace=indent, sectionTitle=comment_contents)
        # Not a change of section so we just return an empty string to append.
        return ''

    def processCoreExtensionComment(self, elt, indent):
        self.processCoreExtension(elt)
        return self.getCoreExtensionComment(indent)
