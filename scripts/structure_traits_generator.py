#!/usr/bin/python3 -i
#
# Copyright (c) 2017-2018 The Khronos Group Inc.
# Copyright (c) 2017-2018 Valve Corporation
# Copyright (c) 2017-2018 LunarG, Inc.
# Copyright 2018-2019, Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):    Mark Young <marky@lunarg.com>
#               Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      This file utilizes the content formated in the
#               automatic_source_generator.py class to produce the
#               generated source code for the loader.

# Based on OpenXR src/scripts/loader_source_generator.py

import os
import re
import sys
from collections import namedtuple

from automatic_source_generator import (AutomaticSourceGeneratorOptions,
                                        regSortFeatures, write)
from enhanced_generator import EnhancedAutoSourceOutputGenerator


# TraitsGeneratorOptions - subclass of AutomaticSourceGeneratorOptions.
class TraitsGeneratorOptions(AutomaticSourceGeneratorOptions):
    def __init__(self,
                 filename=None,
                 directory='.',
                 apiname=None,
                 profile=None,
                 versions='.*',
                 emitversions='.*',
                 defaultExtensions=None,
                 addExtensions=None,
                 removeExtensions=None,
                 emitExtensions=None,
                 sortProcedure=regSortFeatures,
                 prefixText="",
                 **kwargs):
        AutomaticSourceGeneratorOptions.__init__(self, *kwargs,
                                                 filename=filename,
                                                 directory=directory,
                                                 apiname=apiname,
                                                 profile=profile,
                                                 versions=versions,
                                                 emitversions=emitversions,
                                                 defaultExtensions=defaultExtensions,
                                                 addExtensions=addExtensions,
                                                 removeExtensions=removeExtensions,
                                                 emitExtensions=emitExtensions,
                                                 sortProcedure=sortProcedure)
        self.prefixText = prefixText
        # self.prefixText     += '// *********** THIS FILE IS GENERATED - DO NOT EDIT ***********\n'
        # self.prefixText     += '//     See structure_traits_generator.py for modifications\n'
        # self.prefixText     += '// ************************************************************\n'

# TraitsHeaderOutputGenerator - subclass of EnhancedAutoSourceOutputGenerator.


class TraitsHeaderOutputGenerator(EnhancedAutoSourceOutputGenerator):
    """Generate header with type traits using XML element attributes from registry"""

    def __init__(self,
                 errFile=sys.stderr,
                 warnFile=sys.stderr,
                 diagFile=sys.stdout):
        EnhancedAutoSourceOutputGenerator.__init__(
            self, errFile, warnFile, diagFile)

    def outputCopywriteHeader(self):
        # This is included in the prefixText
        pass

    # Override the base class header warning so the comment indicates this file.
    #   self            the SampleImplementationSourceOutputGenerator object
    def outputGeneratedHeaderWarning(self):
        generated_warning = '// *********** THIS FILE IS GENERATED - DO NOT EDIT ***********\n'
        generated_warning += '//     See structure_traits_generator.py for modifications\n'
        generated_warning += '// ************************************************************\n'
        write(generated_warning, file=self.outFile)

    # Call the base class to properly begin the file, and then add
    # the file-specific header information.
    #   self            the ImplementationHeaderOutputGenerator object
    #   gen_opts        the ImplementationHeaderGeneratorOptions object
    def beginFile(self, genOpts):
        EnhancedAutoSourceOutputGenerator.beginFile(self, genOpts)
        preamble = '#pragma once\n'

        write(preamble, file=self.outFile)

    # Write out all the information for the appropriate file,
    # and then call down to the base class to wrap everything up.
    #   self            the SampleImplementationSourceOutputGenerator object
    def endFile(self):
        file_data = '#pragma once\n\n'
        file_data += '// IWYU pragma: private, include "xrtraits/traits/APITraits.h"\n\n'
        file_data += '#include "xrtraits/traits/APITraitsFwd.h"\n\n'
        file_data += 'namespace xrtraits {\n'
        file_data += 'namespace traits {\n'
        file_data += self.indentString + 'namespace detail {\n'
        file_data += self.outputStructureTypeTraits()
        file_data += self.outputTypeEnumStringsTraits()
        file_data += self.outputStructureRecurseTrait()
        file_data += '\n'
        file_data += self.indentString + '} // namespace detail\n\n'
        file_data += self.outputTypeDispatchTrait()
        file_data += '} // namespace traits\n'
        file_data += '} // namespace xrtraits\n\n'
        write(file_data, file=self.outFile)

        # Finish processing in superclass
        EnhancedAutoSourceOutputGenerator.endFile(self)

    def outputGeneratedAuthorNote(self):
        author_note = '//\n'
        author_note += '// Author: Ryan Pavlik <ryan.pavlik@collabora.com>\n'
        author_note += '//\n'
        write(author_note, file=self.outFile)

    def outputStructureTypeTraits(self):
        self.resetCoreExtensionTracking()
        indent = self.indent()
        ret = ''
        structure_types = set()
        for core in [True, False]:
            elts = filter(lambda t: self.isCoreExtensionName(
                t.ext_name) == core and self.isTaggedType(t), self.api_structures)

            for current in elts:
                ret += self.processCoreExtensionComment(current, indent)

                if current.protect_value:
                    ret += '\n#if %s' % current.protect_string

                ret += '\n' + indent
                ret += 'template <> constexpr bool is_xr_tagged_type_impl_v<%s> = true;\n' % current.name
                ret += indent
                ret += 'template <> constexpr const char * xr_type_name_impl_v<%s> = "%s";\n' % (
                    current.name, current.name)
                struct_type = self.getStructureTag(current)
                if struct_type is not None:
                    # Note: Some structure types (mainly the ones ending in "BaseHeader") don't have an enum value for themselves specifically:
                    # presumably they're always intended to just be "abstract" types who take on other concrete types.

                    if struct_type in structure_types:
                        print("WARNING: Already saw structure type", struct_type,
                              "- This is presumably an error in the XML!")
                    else:
                        ret += indent
                        ret += 'template <> constexpr XrStructureType xr_type_tag_impl_v<%s> = %s;\n' % (
                            current.name, struct_type)
                        ret += indent
                        ret += 'template <> constexpr const char * xr_type_name_by_tag_impl_v<%s> = "%s";\n' % (
                            struct_type, current.name)
                        ret += indent
                        ret += 'template <> constexpr bool has_xr_type_tag_impl_v<%s> = true;\n' % current.name
                        structure_types.add(struct_type)
                if current.protect_value:
                    ret += '#endif // %s\n' % current.protect_string
        return ret

    def outputTypeEnumStringsTraits(self):
        self.resetCoreExtensionTracking()
        indent = self.indent()
        indent2 = self.indent(2)
        indent3 = self.indent(3)
        indent4 = self.indent(4)

        ret = ""
        ret += indent + \
            'constexpr const char * structureTypeEnumToTypeNameString(XrStructureType tag) {\n'
        ret += indent2 + 'switch (tag) {\n'

        structure_types = set()
        for core in [True, False]:
            elts = [t for t in self.api_structures if self.isCoreExtensionName(
                t.ext_name) == core and self.hasDefinedStructureTag(t)]

            for current in elts:
                ret += self.processCoreExtensionComment(current, '')
                if current.protect_value:
                    ret += '\n#if %s\n' % current.protect_string
                enum = self.getStructureTag(current)

                if enum in structure_types:
                    print("WARNING: Already saw structure type", enum,
                          "- This is presumably an error in the XML!")
                else:
                    ret += indent3 + 'case %s:\n' % enum
                    ret += indent4 + 'return xr_type_name_by_tag_impl_v<%s>;\n' % enum
                    structure_types.add(enum)
                if current.protect_value:
                    ret += '#endif // %s\n' % current.protect_string

        ret += indent3 + 'default: return nullptr;\n'
        ret += indent2 + '}\n'
        ret += indent + '}\n'
        return ret

    def outputStructureRecurseTrait(self):
        self.resetCoreExtensionTracking()
        xrTaggedTypes = set(
            [t.name for t in self.api_structures if self.hasDefinedStructureTag(t)])

        indent = self.indent()
        indent2 = self.indent(2)
        indent3 = self.indent(3)
        ret = ''
        for core in [True, False]:
            elts = filter(lambda t: self.isCoreExtensionName(
                t.ext_name) == core and self.isTaggedType(t), self.api_structures)

            for current in elts:
                taggedMembers = [
                    member for member in current.members if member.type in xrTaggedTypes and member.pointer_count == 0]
                if len(taggedMembers) == 0:
                    # Skip those with no contained tagged structs
                    continue

                # Raise this error since we removed the code that calls these recursion traits.
                raise RuntimeError(
                    'Tagged members discovered, which violates assumptions in codebase!')
                # ret += self.processCoreExtensionComment(current, indent)

                # if current.protect_value:
                #     ret += '\n#if %s' % current.protect_string

                # ret += '\n'
                # ret += indent + 'template <> constexpr bool struct_has_tagged_members_impl_v<%s> = true;\n' % current.name

                # ret += indent + 'template <> struct visit_tagged_members<%s>\n' % current.name
                # ret += indent + "{\n"
                # ret += indent2 + \
                #     'template <typename T, typename F> static constexpr void visit(T& o, F&& f)\n'
                # ret += indent2 + "{\n"
                # for member in taggedMembers:
                #     ret += indent3
                #     ret += 'f(o.%s);\n' % member.name
                # ret += indent2 + "}\n"

                # ret += indent + "};\n"
                # if current.protect_value:
                #     ret += '#endif // %s\n' % current.protect_string
        return ret

    def outputTypeDispatchTrait(self):
        self.resetCoreExtensionTracking()
        indent = self.indent()
        indent2 = self.indent(2)
        indent3 = self.indent(3)
        ret = ''
        ret += 'template<typename F>\n'
        ret += 'constexpr void typeDispatch(XrStructureType t, F&& f) {\n'
        ret += indent + 'switch (t) {\n'

        structure_types = set()
        for core in [True, False]:
            elts = [t for t in self.api_structures if self.isCoreExtensionName(
                t.ext_name) == core and self.hasDefinedStructureTag(t)]

            for current in elts:
                ret += self.processCoreExtensionComment(current, '')
                if current.protect_value:
                    ret += '\n#if %s\n' % current.protect_string
                enum = self.getStructureTag(current)

                if enum in structure_types:
                    print("WARNING: Already saw structure type", enum,
                          "- This is presumably an error in the XML!")
                else:
                    ret += indent2 + 'case %s:\n' % enum
                    ret += indent3 + 'f(TypeWrapper<%s>{});\n' % current.name
                    ret += indent3 + 'break;\n'
                    structure_types.add(enum)

                if current.protect_value:
                    ret += '#endif // %s\n' % current.protect_string

        ret += indent2 + 'default: break;\n'
        ret += indent + '}\n'
        ret += '}\n'
        return ret
