#!/usr/bin/python3
#
# Copyright 2018, Collabora Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      Converts multi-line C doxygen comments to block (C++-style) comments.

import re
import sys

COMMENT_LINE = re.compile(
    r'(?P<indent>\s*)//(?P<dox>[!/])(?P<body>(?:\s+.*)?)')


def match_comment_line(line):
    match = COMMENT_LINE.match(line)
    if match is None:
        return None
    if match.group("body").startswith(" @{"):
        # Not going to convert things like /// @{
        return None
    if match.group("body").startswith(" Copyright"):
        return None
    if match.group("body").startswith(" SPDX"):
        return None

    return match


class BlockCommentProcessor(object):
    def __init__(self):
        self.done = False
        self.changed = False

    def get_line(self):
        if self.i < len(self.lines):
            ret = self.lines[self.i]
            self.i += 1
            return ret
        return None

    def convert_first_line(self, match):
        self.changed = True
        return "{indent}/*!{body}".format(
            indent=match.group("indent"),
            body=match.group("body"))

    def convert_subsequent_line(self, match):
        self.changed = True
        return "{indent} *{body}".format(
            indent=match.group("indent"),
            body=match.group("body"))

    def generate_last_line(self, first_match):
        self.changed = True
        return "{indent} */".format(indent=first_match.group("indent"))

    def convert_single_line(self, match):
        if match.group('dox') == '/':
            self.changed = True
        return "{indent}//!{body}".format(
            indent=match.group("indent"),
            body=match.group("body"))

    def convert_contiguous_comment_lines(self, first_match):
        """Call with a match result from the COMMENT_LINE regex
        that corresponds to the first line of consecutive single-line
        comments, to convert any additional contiguous comment
        lines into a single block comment."""

        orig_indent = first_match.group("indent")
        while True:
            current_line = self.get_line()
            if current_line is None:
                # Hit EOF.
                # Make the comment's last line then return
                yield self.generate_last_line(first_match)
                return

            current_match = match_comment_line(current_line)

            if not current_match or current_match.group("indent") != orig_indent:
                # OK, hit the first non-comment line (or a change in indentation),
                # so make the comment's last line, spit out this line unchanged,
                # then go back to the outer loop
                yield self.generate_last_line(first_match)
                yield current_line
                return

            # If we're here, then we are in a comment line still
            yield self.convert_subsequent_line(current_match)

    def convert(self, lines):
        self.lines = lines
        self.i = 0

        while not self.done:
            my_lines = [None] * 2
            my_matches = [None] * 2

            my_lines[0] = self.get_line()
            if my_lines[0] is None:
                # ran out of input, all done.
                return

            my_matches[0] = match_comment_line(my_lines[0])
            if not my_matches[0]:
                # Line is not a comment.
                yield my_lines[0]
                continue

            my_lines[1] = self.get_line()
            if my_lines[1] is None:
                # If we run out trying to get the second line of a block,
                # this is a single line comment
                yield self.convert_single_line(my_matches[0])
                return

            my_matches[1] = match_comment_line(my_lines[1])
            if ((not my_matches[1]) or
                    (my_matches[1].group("indent") != my_matches[0].group("indent"))):
                # OK, just a single line comment followed by something else.
                yield self.convert_single_line(my_matches[0])
                yield my_lines[1]
                continue

            if my_matches[1].group("body") == ' @{':
                # Special case: two lines like this
                # /// something, probably @addtogroup
                # /// @{
                # Don't treat as a block.
                yield self.convert_single_line(my_matches[0])
                yield self.convert_single_line(my_matches[1])
                continue

            # If we get here, we already know there are two consecutive comment lines.
            # Hand them off to convert.

            yield self.convert_first_line(my_matches[0])
            yield self.convert_subsequent_line(my_matches[1])
            for processed in self.convert_contiguous_comment_lines(my_matches[0]):
                yield processed


def process_lines(lines):
    obj = BlockCommentProcessor()
    new_lines = '\n'.join([x for x in obj.convert(lines)]) + '\n'
    return obj.changed, new_lines


test_data = """

/// @addtogroup APIhelpers
/// @{


/// Session specialized for OpenGL on Wayland
///
/// @ingroup HandleBacking

	//! Implements @ep{xrCreateProjection}.


	/// Constructor
	/// graphics and viewportConfig are references here because they may not
	/// be null.
	OpenGLWaylandSession(
	    Instance& instance, DynamicVerified<XrSessionCreateInfo const> info,
	    DynamicVerified<XrGraphicsBindingOpenGLWaylandKHR const> binding,
	    System& sys, OpenGLWaylandGraphics& graphics);

""".split('\n')


def test():
    changed, new_data = process_lines(test_data)
    assert(changed)
    print(new_data)


if __name__ == "__main__":
    if len(sys.argv) == 1 or sys.argv[1] == "test":
        test()
        sys.exit(1)

    fn = sys.argv[1]
    outfn = fn
    if len(sys.argv) > 2:
        outfn = sys.argv[2]
    with open(fn, 'r') as target:
        input_data = [line.rstrip() for line in list(target)]

    changed, new_data = process_lines(input_data)
    if changed:
        print("{}: processed, with changes".format(fn))
        with open(outfn, 'w') as target:
            target.write(new_data)
