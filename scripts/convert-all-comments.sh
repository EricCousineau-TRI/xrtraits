#!/bin/bash
# Copyright 2018, Collabora, Ltd.
# Copyright 2016, Sensics, Inc.
# SPDX-License-Identifier: BSL-1.0

runOnDir() {
    find "$1" \( -name "*.c" -o -name "*.cpp" -o -name "*.c" -o -name "*.h" \)| \
        grep -v "\.boilerplate" | \
        while read fn; do
            scripts/convert_comments.py $fn
        done
}

(
cd $(dirname $0)/..
runOnDir src
)
