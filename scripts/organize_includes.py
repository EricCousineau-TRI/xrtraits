#!/usr/bin/python3
#
# Copyright 2018, Collabora Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      Fixes grouping of include lines, esp.
#               when that grouping has changed over time.

import re
import sys
from enum import Enum, unique
from collections import namedtuple

@unique
class IncludeCat(Enum):
    INTERNAL = 0
    LIBRARY = 1
    STANDARD = 2
    POST_INCLUDES = 3

    def getComment(self):
        # self is the member here
        if self == IncludeCat.POST_INCLUDES:
            return None

        return "// %s Includes" % self.getFormattedName()

    def getFormattedName(self):
        # self is the member here
        if self == IncludeCat.POST_INCLUDES:
            return None
        return self.name[:1] +  self.name[1:].lower()

    def match(self, line):
        return re.match(r"// {} [Ii]ncludes.*".format(self.getFormattedName()), line)

    def getNextValue(self):
        # self is the member here
        return self.value + 1

    @classmethod
    def getNext(cls, mem):
        # cls is the enumeration
        if mem == cls.INTERNAL:
            return cls.LIBRARY
        if mem == cls.LIBRARY:
            return cls.STANDARD
        if mem == cls.STANDARD:
            return cls.POST_INCLUDES
        raise RuntimeError("Can't get the next following %s" % mem)

    @classmethod
    def categories(cls):
        """Generates the non-post-includes include categories"""
        # cls is the enumeration
        yield cls.INTERNAL
        yield cls.LIBRARY
        yield cls.STANDARD


NONE_COMMENT = "// - none"

ACCEPTABLE_COMMENTS = set([NONE_COMMENT] + [cat.getComment() for cat in IncludeCat.categories()])

IncludeDelimiters = namedtuple('IncludeDelimiters', ['opener', 'closer'])

class IncludeStyle(Enum):
    Quotes = IncludeDelimiters(opener="\"", closer="\"")
    AngleBrackets = IncludeDelimiters(opener="<", closer=">") # pointy boys

    def getIncludeDirective(self, fn):
        return "".join(["#include ", self.value.opener, fn, self.value.closer])

DECOMPOSE_INCLUDE = re.compile(r"#include (?P<header>[\"<](?P<fn>[^>\"]+)[\">])(?P<trailing>.*)")

class IncludeData(object):
    def __init__(self, cat, style, re_pattern=None, name=None):
        self.category = cat
        self.style = style
        if re_pattern and name:
            raise RuntimeError("Can't specify both a filename and a pattern!")
        self.re = None
        self.name = None
        if re_pattern:
            self.re = re.compile(re_pattern)
        if name:
            self.name = name

    def matchesFilename(self, fn):
        if self.re:
            return bool(self.re.match(fn))
        if self.name:
            return fn == self.name
        raise RuntimeError("Can't match against an IncludeData with neither name nor pattern!")

    def regenerateFromMatch(self, match):
        return self.category, "".join([
            self.style.getIncludeDirective(match.group("fn")),
            match.group("trailing")])


KNOWN_INCLUDES = [
    IncludeData(IncludeCat.LIBRARY, IncludeStyle.Quotes, name="OpenXR_Wrapper.h"),
    IncludeData(IncludeCat.LIBRARY, IncludeStyle.Quotes, name="GSL.h"),
    IncludeData(IncludeCat.INTERNAL, IncludeStyle.AngleBrackets, re_pattern=r"cpp17/.*"),
]

DEFAULT_LIBRARY_DATA = IncludeData(IncludeCat.LIBRARY, IncludeStyle.AngleBrackets)

def processIncludeLine(origCat, line):
    """Returns category, line for a supplied input line, or None if it isn't an include line."""
    if len(line) == 0 or line == NONE_COMMENT:
        return None, None

    match = DECOMPOSE_INCLUDE.search(line)
    if not match:
        # Not an include line? Just return the same cat
        return origCat, line

    fn = match.group("fn")

    # First try the explictly-listed includes
    matchingData = list(filter(lambda x: x.matchesFilename(fn), KNOWN_INCLUDES))
    if len(matchingData) > 0:
        return matchingData[0].regenerateFromMatch(match)

    if match.group("header").startswith("<") and origCat == IncludeCat.INTERNAL:
        # <> means LIBRARY or STANDARD - if it's listed as INTERNAL, move it to LIBRARY.
        return DEFAULT_LIBRARY_DATA.regenerateFromMatch(match)

    # fallback to existing category
    return origCat, line

def isPlausibleIncludeLine(line):
    if len(line) == 0:
        return True
    if line == NONE_COMMENT or line.startswith("#"):
        return True
    return False

class SourceFile(object):
    def __init__(self, lines = None):
        self.valid = False
        self.verbose = False
        self.changed = False
        if lines:
            self.parse(lines)

    def parse(self, lines):
        self.lines = lines
        self.line_nums = dict([(cat, None) for cat in IncludeCat])
        self.new_lines = []

        self.valid = True
        seeking = IncludeCat.INTERNAL
        for num, line in enumerate(lines):
            sought_comment = seeking.getComment()
            if self.verbose:
                print("Line: {}  Seeking: {}  Sought Comment: {} Contents: {}".format(num, seeking, sought_comment, line))
            if seeking != IncludeCat.POST_INCLUDES:
                # Looking for a comment

                if seeking.match(line):
                    # found the comment we sought
                    seeking = self.record_location_and_advance(seeking, num)

                elif seeking != IncludeCat.INTERNAL and self.__isLineFailure(line):
                    # If we already found INTERNAL, have to check for #if
                    return

            else:
                # seeking is IncludeCat.POST_INCLUDES, so
                # looking for first non-# line
                if not isPlausibleIncludeLine(line):
                    # found it, all done
                    self.record_location_and_advance(seeking, num)
                    return
        print("Didn't find all the markers")
        self.valid = False

    def record_location_and_advance(self, category, line_num):
        if self.line_nums[category]:
            raise RuntimeError("Already recorded line number for %s" % category)
        if self.verbose:
            print("Found {} at line {}".format(category, line_num))
        self.line_nums[category] = line_num
        if category != IncludeCat.POST_INCLUDES:
            return IncludeCat.getNext(category)

    def process(self):

        new_lines = []
        first_include_comment = self.line_nums[IncludeCat.INTERNAL]

        new_lines.extend(self.lines[:first_include_comment])

        new_include_lines = dict([(cat, []) for cat in IncludeCat.categories()])

        # Iterate through existing includes, sorting them
        for cat in IncludeCat.categories():
            start_line = self.line_nums[cat] + 1
            end_line = self.line_nums[IncludeCat.getNext(cat)]

            for line in self.lines[start_line:end_line]:
                new_cat, new_line = processIncludeLine(cat, line)
                if new_cat is None:
                    continue
                if new_cat != cat or new_line != line:
                    self.changed = True
                new_include_lines[new_cat].append(new_line)

        # Iterate through sorted include categories, outputting them
        for cat in IncludeCat.categories():
            new_lines.append(cat.getComment())
            this_group_lines = new_include_lines[cat]
            if len(this_group_lines) == 0:
                new_lines.append(NONE_COMMENT)
            else:
                new_lines.extend(this_group_lines)
            new_lines.append("")


        first_non_include_line = self.line_nums[IncludeCat.POST_INCLUDES]
        new_lines.extend(self.lines[first_non_include_line:])

        self.new_lines = new_lines

    def getProcessed(self):
        lines = self.new_lines
        if not self.valid:
            lines = self.lines
        return "\n".join(lines) + "\n"

    def __isLineFailure(self, line):
        # Seeing any ifdef in the include range means we can't handle this entirely ourselves.
        # Any C-style comment is also disqualifying
        if line.startswith("#if") or "/*" in line:
            self.valid = False
            return True
        if "//" in line and line.strip() not in ACCEPTABLE_COMMENTS and not line.startswith("#include"):
            # We don't bother trying to process unrecognized comments in the includes either
            print("Found non-acceptable comment")
            self.valid = False
            return True
        return False


dummyData = [ x + "\n" for x in """

// Internal Includes
#include "exceptions/ValidationError.h"

// Library Includes
#include "GSL.h"

// Standard Includes
// - none

namespace xrtraits {

""".split("\n")]


def test():
    for cat in IncludeCat:
        print(cat.getComment())
    source = SourceFile(dummyData)
    print("\n".join(source.getProcessed()))

if __name__ == "__main__":
    if sys.argv[1] == "test":
        test()
        sys.exit(1)
    source = SourceFile()
    #source.verbose = True
    fn = sys.argv[1]
    outfn = fn
    if len(sys.argv) > 2:
        outfn = sys.argv[2]
    with open(fn, 'r') as target:
        source.parse([line.rstrip() for line in list(target)])
    if source.valid:
        source.process()
        if source.changed:
            print("{}: processed, with changes".format(fn))
            with open(outfn, 'w') as target:
                target.write(source.getProcessed())
    else:
        print("{}: NOT PROCESSED - includes too complex".format(fn))
