
# Copyright 2018-2019, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

find_program(LLVM_COV_COMMAND
    NAMES
    llvm-cov
    llvm-cov-9
    llvm-cov-8
    llvm-cov-7
    llvm-cov-6.0)
find_program(LLVM_PROFDATA_COMMAND
    NAMES
    llvm-profdata
    llvm-profdata-9
    llvm-profdata-8
    llvm-profdata-7
    llvm-profdata-6.0)

cmake_dependent_option(BUILD_COVERAGE "Enable LLVM-based code coverage reporting?" ON
    "XRTRAITS_IS_STANDALONE_PROJECT" OFF)

if(BUILD_COVERAGE)
    set(no_coverage_reasons)
    if(NOT "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
        string(APPEND no_coverage_reasons "Compiler is not Clang. ")
    endif()
    if(NOT "${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        string(APPEND no_coverage_reasons "Build type is not Debug. ")
    endif()
    if(NOT LLVM_COV_COMMAND)
        string(APPEND no_coverage_reasons "llvm-cov not found. ")
    endif()
    if(NOT LLVM_PROFDATA_COMMAND)
        string(APPEND no_coverage_reasons "llvm-profdata not found. ")
    endif()

    if(no_coverage_reasons)
        set(BUILD_COVERAGE OFF)
        xrtraits_standalone_message(STATUS "Note: Test coverage not available because: ${no_coverage_reasons}")
    endif()
endif()

if(BUILD_COVERAGE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-instr-generate -fcoverage-mapping")

    find_program(CXXFILT_COMMAND
        NAMES c++filt)
    function(xrtraits_add_coverage COVTARGET TARGET)
        xrtraits_standalone_message(STATUS "Adding target '${COVTARGET}' to run output of target '${TARGET}' and report on code coverage.")
        set(RAW_FILE "${CMAKE_CURRENT_BINARY_DIR}/coverage-${TARGET}.profraw")
        set(DATA_FILE "${CMAKE_CURRENT_BINARY_DIR}/coverage-${TARGET}.profdata")
        set(REPORT_DIR "${CMAKE_CURRENT_BINARY_DIR}/${COVTARGET}")
        set(CXXFILT_ARGS)
        if(CXXFILT_COMMAND)
            set(CXXFILT_ARGS -Xdemangler ${CXXFILT_COMMAND} -Xdemangler -n)
        endif()
        add_custom_command(OUTPUT "${RAW_FILE}"
            COMMAND
            LLVM_PROFILE_FILE=${RAW_FILE} $<TARGET_FILE:${TARGET}>
            DEPENDS $<TARGET_FILE:${TARGET}>
            COMMENT "Running ${TARGET} with coverage enabled"
            USES_TERMINAL
            VERBATIM)
        add_custom_command(OUTPUT "${DATA_FILE}"
            COMMAND
            ${LLVM_PROFDATA_COMMAND} merge -sparse ${RAW_FILE} -o ${DATA_FILE}
            DEPENDS ${RAW_FILE}
            COMMENT "Merging coverage data for ${TARGET}"
            VERBATIM)

        # Any extra arguments are treated as targets whose source file coverage we are interested in.
        set(ABSOLUTE_SOURCES)
        foreach(sourcetarget ${ARGN})
            get_target_property(sourcedir ${sourcetarget} SOURCE_DIR)
            get_target_property(sources ${sourcetarget} SOURCES)
            foreach(source ${sources})
                if(IS_ABSOLUTE ${source})
                    list(APPEND ABSOLUTE_SOURCES ${source})
                else()
                    list(APPEND ABSOLUTE_SOURCES ${sourcedir}/${source})
                endif()
            endforeach()
        endforeach()

        add_custom_target(${COVTARGET}
            ${LLVM_COV_COMMAND}
            show
            $<TARGET_FILE:${TARGET}>
            -instr-profile=${DATA_FILE}
            -show-line-counts-or-regions
            #-show-instantiations=false
            -output-dir=${REPORT_DIR}
            -format=html
            ${CXXFILT_ARGS}
            ${ABSOLUTE_SOURCES}
            DEPENDS "${DATA_FILE}"
            COMMENT "Generating coverage report for ${TARGET} - view ${REPORT_DIR}/index.html to see results"
            VERBATIM)
    endfunction()
else()
    # Dummy impl to avoid needing lots of conditionals.
    function(xrtraits_add_coverage)
    endfunction()
endif()
