# Copyright (c) 2017-2018 The Khronos Group Inc.
# Copyright 2018-2019, Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, _Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Based on openxr repo files:
# src/CMakeLists.txt
# src/impl/CMakeLists.txt

set(LOCAL_SCRIPTS "${CMAKE_CURRENT_SOURCE_DIR}/scripts")

# Path separators ( : or ; ) are not handled well in CMake.
# This seems like a reasonable approach.
if(CMAKE_SYSTEM_NAME STREQUAL "Windows")
    set(XRTRAITS_DELIM ";")
else()
    set(XRTRAITS_DELIM ":")
endif()

# Code shared between multiple "generation" procedures
# where a python script is run with a custom pythonpath
function(xrtraits_internal_xr_python)
    add_custom_command(COMMAND "${CMAKE_COMMAND}" -E env "PYTHONPATH=${XRTRAITS_CODEGEN_PYTHON_PATH}"
            "${PYTHON_EXECUTABLE}" ${ARGN}) # All extra arguments just get passed to add_custom_command (so some will be additional python args)
endfunction()

# run src_genxr.py from official/sample impl
# (inner workings mostly factored out into internal_xr_generate)
function(xrtraits_run_xr_spec_xml_generate _dependency _output)
    set(XRTRAITS_CODEGEN_PYTHON_PATH "$ENV{PYTHONPATH}")
    xrtraits_internal_xr_python(
        # script
        "${OPENXR_SPECSCRIPTS_DIR}/genxr.py"
        # No output
        "-quiet"
        # script args
        -registry "${OPENXR_REGISTRY}" "${_output}"
        # other add_custom_command args
        OUTPUT "${_output}"
        MAIN_DEPENDENCY "${OPENXR_SPECSCRIPTS_DIR}/${_dependency}"
        DEPENDS
        "${OPENXR_REGISTRY}"
        "${OPENXR_SPECSCRIPTS_DIR}/genxr.py"
        "${OPENXR_SPECSCRIPTS_DIR}/generator.py"
        "${OPENXR_SPECSCRIPTS_DIR}/reg.py"
        ${ARGN} # forward extra deps if any
        COMMENT "Generating ${_output} using ${PYTHON_EXECUTABLE} on ${_dependency}")
endfunction()

# run src_genxr.py from OpenXR-SDK
# (inner workings mostly factored out into internal_xr_generate)
function(xrtraits_run_xr_xml_generate _dependency _output)
    set(XRTRAITS_CODEGEN_PYTHON_PATH "${LOCAL_SCRIPTS}${XRTRAITS_DELIM}$ENV{PYTHONPATH}")
    xrtraits_internal_xr_python(
        # script
        "${OPENXR_SDKSCRIPTS_DIR}/src_genxr.py"
        # No output
        "-quiet"
        # script args
        -registry "${OPENXR_REGISTRY}" "${_output}"
        # other add_custom_command args
        OUTPUT "${_output}"
        MAIN_DEPENDENCY "${OPENXR_SDKSCRIPTS_DIRS}/${_dependency}"
        DEPENDS
        "${OPENXR_REGISTRY}"
        "${OPENXR_SDKSCRIPTS_DIR}/src_genxr.py"
        "${OPENXR_SPECSCRIPTS_DIR}/generator.py"
        "${OPENXR_SPECSCRIPTS_DIR}/reg.py"
        ${ARGN} # forward extra deps if any
        COMMENT "Generating ${_output} using ${PYTHON_EXECUTABLE} on ${_dependency}")
endfunction()

# run genxrtraits
# (inner workings mostly factored out into internal_xr_generate)
function(xrtraits_run_local_xr_xml_generate _dependency _output)
    set(XRTRAITS_CODEGEN_PYTHON_PATH "${LOCAL_SCRIPTS}${XRTRAITS_DELIM}$ENV{PYTHONPATH}")
    xrtraits_internal_xr_python(
        # script
        "${LOCAL_SCRIPTS}/genxrtraits.py"
        # No output
        "-quiet"
        # script args
        -registry "${OPENXR_REGISTRY}" "${_output}"
        # other add_custom_command args
        OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${_output}"
        MAIN_DEPENDENCY "${LOCAL_SCRIPTS}/${_dependency}"
        DEPENDS
        "${OPENXR_REGISTRY}"
        "${LOCAL_SCRIPTS}/genxrtraits.py"
        "${LOCAL_SCRIPTS}/enhanced_generator.py"
        "${LOCAL_SCRIPTS}/generator.py"
        "${LOCAL_SCRIPTS}/reg.py"
        "${LOCAL_SCRIPTS}/automatic_source_generator.py"
        ${ARGN} # forward extra deps if any
        COMMENT "Generating ${_output} using ${PYTHON_EXECUTABLE} on ${_dependency}")
endfunction()
