# Main Page {#mainpage}

## Introduction

- Source, issue tracker, merge requests: <https://gitlab.freedesktop.org/monado/utilities/xrtraits>
- Latest online docs: <https://monado.pages.freedesktop.org/utilities/xrtraits/>
- Latest online code coverage data <https://monado.pages.freedesktop.org/utilities/xrtraits/coverage/>

In this documentation:

- [README](README.md) (includes Code of Conduct)
- [CONTRIBUTING](CONTRIBUTING.md)
- An introduction to the key ideas of this library can be seen at @ref DynamicTypes.
- The "Modules" link above shows a partial breakdown of the functionality of this library into logical groups.
- The "Files" link above is likely a more up-to-date (and thus useful) division of functionality into groups.

## General coding guidelines

The term "classes" and "types" in this section refer to `class` and `struct` types,
`class` and `struct` type templates,
and type aliases (created with the keyword `using`)
that are _not_ involved in compile-time meta-programming.
Rephrased, those terms in this section refer to types that are
intended for direct usage through instantiation,
or through instantiation of a derived type.
This also somewhat extends to naming of type-manipulation-related functions,
such as the various "cast" functions:
since they resemble functionality provided by the C++ standard implementation,
they are named using a similar pattern to their standard counterparts,
e.g. xrtraits::required_verified_cast&lt;T&gt;().

Handling of input OpenXR structures with `type` and `next` members is supported
with verification and type-safety utilities,
see the page on [Dynamic Typing and OpenXR Structs](TypeVerification) for details.

Loose coupling, high cohesion.

Avoid writing by hand things that can be generated from the specification.
See the Python files under `scripts/` for examples and starting places that use
infrastructure developed for the loader to make code generation easier.

This is not intended to be a comprehensive "style guide".
For those needs, see widely-used references such as "Effective Modern C++" by Scott Meyers
(as well as the non-outdated recommendations from the earlier "Effective C++" books)
and the [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines).

In terms of bikeshed color, match the surroundings.
It may help to humbly remember that maintaining code is roughly
twice as difficult as writing it in the first place.
Be kind to those who will follow you, potentially including your future self.

Use `clang-format` and `git clang-format` compulsively.
Try not to push commits that would change if you formatted them.
(`git clang-format` will only format the portions of files modified by staged changes,
so it avoids "collateral damage" from formatting in code that might not be fully formatted.
But, this is a young project so this shouldn't be as common of a concern
until a new clang-format release makes our existing `.clang-format` file behave differently.)

Use the [file-boilerplate tool](https://gitlab.collabora.com/rpavlik/file-boilerplate)
when you want to create a new source or header file.

Mark "todos" as follows, so Doxygen and other tools can easily extract them:

@verbatim
/// @todo short description here
@endverbatim

Check the Doxygen output from time to time during development to make sure it looks how you expected it to.

### Naming

- `TypesLookLikeThis`
- `functionsLookLikeThis` - exceptions are the free "cast" functions, as noted above.
- `privateDataMembersLookLikeThis_` - note the trailing underscore,
  to denote private data members with minimal visual clutter and minimal conflict.
  (Names with leading underscores are reserved to the language implementation.)
- Code files (`.cpp` and `.h`) **should** be named with
  their filename stem equal to the main class declared/defined within,
  without any namespace prefix.

## Copyright and License for this index.md page

For this page only:

> Copyright 2018-2019 Collabora, Ltd.
>
> SPDX-License-Identifier: BSL-1.0
