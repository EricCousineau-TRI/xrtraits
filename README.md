# xrtraits

C++ type utilities and type traits for the [Khronos OpenXR API][openxr].

Maintained as a part of the [Monado project](http://monado.freedesktop.org).

[openxr]: https://khronos.org/openxr

- Doxygen output for latest `master`: <https://monado.pages.freedesktop.org/utilities/xrtraits/>
- Testing code coverage report for latest `master`: <https://monado.pages.freedesktop.org/utilities/xrtraits/coverage/>

## Quick Start

[fmtlib][] is a required dependency if you're using the exceptions-using code:
you can include it as a submodule in your project,
or have it installed elsewhere, as long as `find_package(fmt)` can find it or there
is a CMake target called `fmt`.
(If you don't need the exceptions-using code, everything else is header-only.)

[CMake][] 3.9 or newer is a highly-recommended dependency:
without it, you'll have to use your own build system to set up include paths
and build the object files.
Doable, but if you're using CMake, just use this as a submodule/vendored project.

Python 3.4+ is a suggested dependency: having it is required to re-generate the files
that are cached in `cached-generated/`.

The [OpenXR-SDK][] source repository
(or at least a subset of its contents: the `xr.xml` file and many of the `.py` scripts)
is also required if you want to re-generate the files that are cached in `cached-generated/`.
If the SDK source is cloned into a sibling directory of this directory
or of the overall source directory, it should automatically be found.

The "best", most complete version of this library requires C++17.
However, not all parts of it require that.
Some of these are guarded by standards version checks,
but probably not all of them.
The main C++17 feature that is used is `if constexpr` because it simplifies generic code so much.
(Though, if determined to be important, this could be reworked to use partial specialization,
and has been in some key locations.)
The rest of the code is more C++14.

### Optional and Test Prerequisites

HTML documentation is generated with Doxygen (and graphviz) if you have it,
by building the `doc` target.

- On Debian and relatives, `sudo apt install doxygen graphviz` installs those.

Additionally, [clang-format][] and [clang-tidy][] are highly recommended if you want to contribute.

- On Debian and relatives, `sudo apt install clang-format clang-tidy` will get you whatever version is default.
- However, I'd recommend using the latest version you have access to,
  since newer version improve standards support and features.
  On Debian-related distros, append something like `-6.0`, `-7`, `-8` to the two package names above
  to choose a particular version.
- See also the [LLVM apt repos](http://apt.llvm.org/) for nightly packages for Debian and Ubuntu.

Code coverage reports are available if building in "Debug" mode with Clang,
and if you have the right additional related LLVM tools (`llvm-cov` and `llvm-profdata`).
`c++filt` is optional but highly recommended if you're going to look at coverage reports.
On Debian and friends, the two LLVM tools are in the
the `llvm`, `llvm-6.0`, `llvm-7`, `llvm-8`, etc. packages,
Additionally, `c++filt` is in the `binutils` Debian package which is required by Clang,
so you probably already have that.

Finally, the tests are built with the [Catch2][] test framework.
A copy of the single-header-include is vendored into this source tree,
but if you would rather use a different (preferably newer) version or
your copy of the `xrtraits` source tree lacks this file for whatever reason,
the build does search for this on your system first.
It first tries looking for a CMake config file,
such as the one `vcpkg` installs (with `vcpkg install catch2`),
and if that fails, it will look for a `catch2/catch.hpp` header file.
The vendored copy is provided as a fall-back location for this header,
so if it finds a system one it will prefer that unless told otherwise by
setting `CATCH2_INCLUDE_DIR` in CMake.

## Primary Authors and Contributors

- **Ryan Pavlik** - *Initial work* - `rpavlik` on many platforms, `ryan.pavlik` on the others

## License

All files created for this project are licensed under the
Boost Software License v1.0 (BSL-1.0).

By the nature of this project, some files are derived from ones in the OpenXR-SDK
or are generated based on its contents.
The files based on those found in the OpenXR-SDK,
including the generated `APITraitsImpl.h` file,
are subject to the license found there: Currently, this means Apache-2.0.

All non-external files shall have an SPDX tag indicating their license,
as well as a copyright statement.

Reasonable efforts have been made to ensure license compatibility and broad usability,
but I am not a lawyer, so be mindful of licenses in this library, its dependencies,
and any other packages you might use as you develop your own software.

## Acknowledgments

- [Collabora](https://collabora.com) for supporting Ryan's development and maintenance of this code
  in the course of his work.
- Thanks to PurpleBooth's excellent template/advice for structuring a
  [README](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)

[Catch2]: https://github.com/catchorg/Catch2
[clang-format]: https://releases.llvm.org/7.0.0/tools/clang/docs/ClangFormat.html
[clang-tidy]: http://releases.llvm.org/7.0.0/tools/clang/tools/extra/docs/clang-tidy/index.html
[cmake-build]: https://cmake.org/cmake/help/v3.12/manual/cmake.1.html#build-tool-mode
[cmake-generate]: https://cmake.org/cmake/help/v3.12/manual/cmake.1.html
[CMake]: https://cmake.org
[CTest]: https://cmake.org/cmake/help/v3.12/manual/ctest.1.html
[fmtlib]: http://fmtlib.net/
[OpenXR-SDK]: https://github.com/KhronosGroup/OpenXR-SDK

## Contributing, Code of Conduct

See `CONTRIBUTING.md` for details of contribution guidelines.

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms.

We follow the standard freedesktop.org code of conduct,
available at <https://www.freedesktop.org/wiki/CodeOfConduct/>,
which is based on the [Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

- First-line Monado project contacts:
  - Ryan Pavlik <ryan.pavlik@collabora.com>
  - Jakob Bornecrantz <jakob@collabora.com>
- freedesktop.org contacts: see most recent list at <https://www.freedesktop.org/wiki/CodeOfConduct/>

---

## Copyright and License for this README.md file

For this file only:

> Copyright 2018-2019 Collabora, Ltd.
> Code of Conduct section: excerpt adapted from the [Contributor Covenant](https://www.contributor-covenant.org), version 1.4.1,
> available at <https://www.contributor-covenant.org/version/1/4/code-of-conduct.html>,
> and from the freedesktop.org-specific version of that code,
> available at <https://www.freedesktop.org/wiki/CodeOfConduct/>
>>
> SPDX-License-Identifier: CC-BY-4.0
