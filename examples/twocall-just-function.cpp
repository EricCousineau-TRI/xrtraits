// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include <iostream>
#include <xrtraits/TwoCall.h>

using xrtraits::doTwoCall;

int main()
{
	auto apiLayers =
	    doTwoCall<XrApiLayerProperties>(xrEnumerateApiLayerProperties);
	std::cout << "Enumerated layers:\n";
	for (const auto& prop : apiLayers) {
		std::cout << prop.layerName << " - " << prop.description
		          << std::endl;
	}
	return 0;
}
