// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include <iostream>
#include <xrtraits/InitXrType.h>
#include <xrtraits/TwoCall.h>

using xrtraits::doTwoCallWithSizeHint;
using xrtraits::Initialized;

int main()
{
	uint32_t numViews = 2; // Previously initialized as required.
	XrSpace local;         // Previously initialized
	XrSession session;     // Previously initialized
	XrTime frameTime;      // Previously initialized

	Initialized<XrViewState> viewState; // Output
	Initialized<XrViewLocateInfo> viewLocateInfo{frameTime, local}; // Input

	auto views = doTwoCallWithSizeHint<XrView>(
	    numViews, xrLocateViews, session, &viewLocateInfo, &viewState);
	return 0;
}
