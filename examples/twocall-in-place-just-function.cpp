// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include <iostream>
#include <vector>
#include <xrtraits/TwoCall.h>

using xrtraits::doTwoCallInPlace;

int main()
{
	std::vector<XrApiLayerProperties> apiLayers;
	XrResult result =
	    doTwoCallInPlace(apiLayers, xrEnumerateApiLayerProperties);

	if (XR_SUCCEEDED(result)) {
		std::cout << "Enumerated layers:\n";
		for (const auto& prop : apiLayers) {
			std::cout << prop.layerName << " - " << prop.description
			          << std::endl;
		}
	} else {
		std::cerr << "Got unsuccessful result: " << result << std::endl;
	}

	return 0;
}
