// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include <iostream>
#include <xrtraits/TwoCall.h>

using xrtraits::doTwoCall;

int main()
{

	auto extensions = doTwoCall<XrExtensionProperties>(
	    xrEnumerateInstanceExtensionProperties, nullptr);

	for (const auto& prop : extensions) {
		std::cout << prop.extensionName << std::endl;
	}
	return 0;
}
